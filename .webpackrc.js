import path from 'path';
export default {
  entry: "src/index.js",
  extraBabelPlugins: [
    ["import", { libraryName: "antd", libraryDirectory: "es", style: true }]
  ],
  env: {
    development: {
      extraBabelPlugins: [
        "dva-hmr"
      ]
    }
  },
  alias: {
    '@': path.resolve(__dirname, 'src')
  },
  proxy: {
    "/api": {
      target: "http://localhost:1088",
      changeOrigin: true,
      pathRewrite: { "^/api" : "" }
    }
  },
  ignoreMomentLocale: true,
  theme: "./src/theme.js",
  html: {
    template: "./src/index.ejs"
  },
  lessLoaderOptions: {
    javascriptEnabled: true,
  },
  disableDynamicImport: false,
  publicPath: "/",
  hash: true
}
