
const env = process.env.NODE_ENV;

if('development' === env) {
  //开发模式
  window.fileUrl = 'http://localhost:1088';
  window.baseUrl = 'http://localhost:8000';
} else {
  window.fileUrl = 'http://api.loozb.com';
  window.baseUrl = 'http://csl.loozb.com';
}

//前端加密公钥，有效期365天，2018年7月5日 生成
window.publicKey = `
  -----BEGIN PUBLIC KEY-----
  MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCOCFRAJyLMAzXus2Mw/X6ECCJi
  AiylORh2C4TdRKZJ1kILqNz9/YiSnTFtgjUEBc/sE43sMnpSNw1TW3irybZ/XQ0q
  tWshI7jtB9ynUwaRAiGcFXx6rm78W0jHySi0yk01vT6DmSvfaeyFFh8B7ZM2fFED
  y+h8dxa8B+5BXIjErwIDAQAB
  -----END PUBLIC KEY-----
  `


//其他一些常量配置

//1.表格默认大小
window.tableSize = 'middle';
window.table_height = document.documentElement.clientHeight - 240;


//审批流程常量定义
//请假申请
window.LEAVE_PROCESS = 'leaveProcess';

//流程节点
//请假-员工申请
window.USERTASK1 = 'usertask1'; 
//请假-经理审批
window.USERTASK2 = 'usertask2';
//请假-总监审批
window.USERTASK3 = 'usertask3';
//请假-人事审批
window.USERTASK4 = 'usertask4';

Date.prototype.format = function (format) {
  var o = {
    "M+": this.getMonth() + 1, //month
    "d+": this.getDate(),    //day
    "h+": this.getHours(),   //hour
    "m+": this.getMinutes(), //minute
    "s+": this.getSeconds(), //second
    "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
    "S": this.getMilliseconds() //millisecond
  }
  if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
    (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o) if (new RegExp("(" + k + ")").test(format))
    format = format.replace(RegExp.$1,
      RegExp.$1.length == 1 ? o[k] :
        ("00" + o[k]).substr(("" + o[k]).length));
  return format;
}
