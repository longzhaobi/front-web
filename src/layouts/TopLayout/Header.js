import React from 'react';
import { routerRedux } from 'dva/router';
import { Menu, Icon, Avatar, Dropdown, Tag, message, Spin } from 'antd';
import moment from 'moment';
import groupBy from 'lodash/groupBy';
import styles from './Header.less'
const { SubMenu } = Menu;
import NoticeIcon from '@/components/NoticeIcon';
import Feedback from '../Feedback';
// import RightSidebar from '@/components/Sidebar';
import RightHeader from '../Common/RightHeader'

import { Link } from 'dva/router';

export default class Header extends React.PureComponent {
  constructor(props) {
    super(props);
    this.permission = localStorage.permission ?  localStorage.permission.split(",") : [];
    this.state = {
      collapse: false,
    }
  }

  isAuth = (perm) => {

    let flag = false;
    for (let p of this.permission) {
      if(p === perm + ':view') {
        flag = true;
        break;
      }
    }
    return flag;
  }

  getTopMenu = menus => menus.map(item => {
    if (!item.path || item.hide) {
      //如果没有name或者为隐藏路由，则不显示
      return null;
    }

    //如果路由对象有name属性，则进行权限判断，否则放行
    if(item.key) {
      if(!this.isAuth(item.key)) {
        return null;
      }
    }
    let itemPath;
    if (item.path.indexOf('http') === 0) {
      itemPath = item.path;
    } else {
      itemPath = `/${item.path || ''}`.replace(/\/+/g, '/');
    }
    if (item.children && item.children.some(child => child.name)) {
      return (
        <SubMenu
          title={
            item.icon ? (
              <span>
                <Icon type={item.icon} />
                <span>{item.name}</span>
              </span>
            ) : item.name
          }
          key={item.path}
        >
          {this.getTopMenu(item.children, itemPath)}
        </SubMenu>
      );
    }

    const icon = item.icon && <Icon type={item.icon} />;
    return (
      <Menu.Item key={item.path}>
        {
          /^https?:\/\//.test(itemPath) ? (
            <a href={itemPath} target={item.target}>
              {icon}<span>{item.name}</span>
            </a>
          ) : (
            <Link
              to={itemPath}
              target={item.target}
              replace={itemPath === this.props.location.pathname}
            >
              {icon}<span>{item.name}</span>
            </Link>
          )
        }
      </Menu.Item>
    )
  })

  render () {
    const { menuData} = this.props;
    return (
      <div className={styles.header}>
        {/* <RightSidebar collapse={this.state.collapse} togglerContent={this.togglerContent}/> */}
        <div className={styles.left}>
          <Menu
            mode="horizontal"
            defaultSelectedKeys={['dashboard']}
            style={{ lineHeight: '60px',background:'#1890FF' }}
          >
            <Menu.Item key="logo">
              <Link to="/">
                <img src="https://gw.alipayobjects.com/zos/rmsportal/iwWyPinUoseUxIAeElSx.svg" alt="logo" />
                <span style={{fontSize:20, color:'#fff',marginLeft:10}}>中后台项目基础后台模版</span>
              </Link>
            </Menu.Item>
            {
              this.getTopMenu(menuData)
            }
          </Menu>
        </div>
        <RightHeader {...this.props}/>
      </div>
    )
  }

}
