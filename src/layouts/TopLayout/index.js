import React from 'react';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';
import { connect } from 'dva';
import { Route, Redirect, Switch } from 'dva/router';
import { getMenuData } from '../../common/menu';
import { getRoutes } from '../../utils/utils';
import Header from './Header';
import Content from './Content';
import NotFound from '@/routes/Exception/404';
import Breadcrumb from '@/components/Breadcrumb';
import WithInit from '../../hocs/WithInit'

/**
 * 根据菜单取得重定向地址.
 */
const redirectData = [];
const getRedirect = (item) => {
  if (item && item.children) {
    if (item.children[0] && item.children[0].path) {
      redirectData.push({
        from: `/${item.path}`,
        to: `/${item.children[0].path}`,
      });
      item.children.forEach((children) => {
        getRedirect(children);
      });
    }
  }
};
getMenuData().forEach(getRedirect);

@connect(({ app, loading}) => ({
  app,
  loading
}))
@WithInit
class BasicLayout extends React.PureComponent {
  static childContextTypes = {
    location: PropTypes.object,
    breadcrumbNameMap: PropTypes.object,
  }
  constructor(props) {
		super(props);
  }

  getChildContext() {
    const { location, routerData } = this.props;
    return {
      location,
      breadcrumbNameMap:this.getBreadcrumbNameMap(getMenuData(), routerData),
    };
  }

  getBashRedirect = () => {
    // According to the url parameter to redirect
    // 这里是重定向的,重定向到 url 的 redirect 参数所示地址
    const urlParams = new URL(window.location.href);
    const redirect = urlParams.searchParams.get('redirect');
    // Remove the parameters in the url
    if (redirect) {
      urlParams.searchParams.delete('redirect');
      window.history.replaceState(null, 'redirect', urlParams.href);
    } else {
      return '/dashboard/analysis';
    }
    return redirect;
  }

  getPageTitle() {
    const { routerData, location } = this.props;
    const { pathname } = location;
    let title = 'Ant Design Pro';
    if (routerData[pathname] && routerData[pathname].name) {
      title = `${routerData[pathname].name} - Ant Design Pro`;
    }
    return title;
  }

  getBreadcrumbProps = () => {
    return {
      routes: this.props.routes || this.context.routes,
      params: this.props.params || this.context.params,
      location: this.props.location || this.context.location,
      breadcrumbNameMap: this.props.breadcrumbNameMap || this.context.breadcrumbNameMap,
    };
  };

  getBreadcrumbNameMap = (menuData, routerData) => {
    const result = {};
    const childResult = {};
    for (const i of menuData) {
      const path = '/' + i.path;
      if (!routerData[path]) {
        result[path] = i;
      }
      if (i.children) {
        Object.assign(childResult, this.getBreadcrumbNameMap(i.children, routerData));
      }
    }
    return Object.assign({}, routerData, result, childResult);
  };

  render () {
    const { routerData, match, location, app, dispatch, loading } = this.props;
    const breadcrumbDate = this.getBreadcrumbProps();
    const bashRedirect = this.getBashRedirect();
    const { menuStyle, user, notices, notifyCount } = app;
    const fetchingNotices = loading.effects['app/fetchNotices'];
    const tasParams = {
      ...this.props.routerData[location.pathname],
      keys: location.pathname,
      location,
      dispatch,
      match,
    }
    return (
      <DocumentTitle title={this.getPageTitle()}>
        <div>
          <Header dispatch={dispatch}  menuData={getMenuData()} location={location} user={user} notices={notices} fetchingNotices={fetchingNotices} notifyCount={notifyCount}/>
          {/* <Sidebar app={app} menuStyle = {menuStyle} dispatch = {dispatch} location = {location} menuData={getMenuData()}/> */}
          <Breadcrumb {...breadcrumbDate} menuStyle='none' />
          <Content menuStyle='none'>
            <Switch>
              {
                redirectData.map(item =>
                  <Redirect key={item.from} exact from={item.from} to={item.to} />
                )
              }
              {
                getRoutes(match.path, routerData).map(item =>
                  (
                    <Route
                      exact={item.exact}
                      key={item.path}
                      path={item.path}
                      component={item.component}
                    />
                  )
                )
              }
              <Redirect exact from="/" to={bashRedirect} />
              <Route render={NotFound} />
            </Switch>
          </Content>
        </div>
      </DocumentTitle>
    )
  }
}

export default BasicLayout;
