import React, { Component } from 'react';
import { Form, Button ,message, Input, Modal } from 'antd';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 19 },
};

@Form.create()
class Feedback extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      loading: false,
    };
  }

  showModelHandler = (e) => {
    if (e) e.stopPropagation();
    this.setState({
      visible: true,
      loading: false,
    });
  };

  hideModelHandler = () => {
    this.setState({
      visible: false,
      loading: false,
    });
  };

  okHandler = () => {
    const { dispatch } = this.props;
    this.props.form.validateFields((err, params) => {
      if (!err) {
        Modal.confirm({
          title: '系统提示',
          content: '确定提交反馈信息吗？',
          okText: '确定',
          cancelText: '取消',
          onOk:() => {
            this.setState({loading: true});
            dispatch({
              type: 'app/feedback',
              payload: {...params}
            }).then((data) => {
              this.setState({loading: false});
              if(data) {
                this.props.form.resetFields();
                message.info('操作成功')
              }
            })
          }
        });
      }
    })
  };

  render() {
    const { children, record, form } = this.props;

    const { loading } = this.state;

    const { getFieldDecorator } = form;

    return (
      <span>
        <span onClick={this.showModelHandler}>
          { children }
        </span>
        <Modal
          title='意见反馈'
          visible={this.state.visible}
          width={700}
          maskClosable={false}
          onOk={this.okHandler}
          onCancel={this.hideModelHandler}
          footer = {[
            <Button key="back" type="ghost" size="large" onClick={this.hideModelHandler}>取消</Button>,
            <Button key="submit" type="primary" disabled={loading} loading={loading} onClick={this.okHandler} size="large" >
              提交
            </Button>
          ]}
        >
          <Form layout='horizontal' onSubmit={this.onSubmit}>
            <FormItem
              {...formItemLayout}
              label="标题"
            >
            {getFieldDecorator('title', {
              rules: [
                 { required: true, message: '标题必须输入' },
                 { max: 100, message: '标题不能超过100字符' }
               ],
               initialValue: ''
             })(
              <Input type="text" placeholder="意见标题" style={{width:'100%'}}/>
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="意见内容"
            >
              {getFieldDecorator('content', {
                rules: [
                   { required: true, message: '意见内容必须输入' },
                   { max: 500, message: '意见内容不能超过500字符' }
                 ],
                 initialValue: ''
               })(
                <Input.TextArea autosize={{ minRows: 6, maxRows: 6 }} placeholder="请输入意见内容" />
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="联系方式"
              help="联系方式，可以为空"
            >
              {getFieldDecorator('description', {
                rules: [
                   { max: 50, message: '联系方式不能超过50个字符' }
                 ],
                 initialValue: ''
               })(
                <Input type="textarea" placeholder="请输入联系方式" />
              )}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}

export default Feedback;
