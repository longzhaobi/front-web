import React from 'react';

import { Layout, Menu, Icon, Avatar, Dropdown, Tag, message, Spin } from 'antd';
import NoticeIcon from '@/components/NoticeIcon';
import Feedback from '../Feedback';
import styles from './RightHeader.less';
import moment from 'moment';
export default class RightHeader extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  getNoticeData() {
    const { notices = [] } = this.props;
    if (notices.length === 0) {
      return {};
    }
    const newNotices = notices.map((notice) => {
      const newNotice = { ...notice };
      if (newNotice.datetime) {
        newNotice.datetime = moment(notice.datetime).fromNow();
      }
      // transform id to item key
      if (newNotice.id) {
        newNotice.key = newNotice.id;
      }
      if (newNotice.extra && newNotice.status) {
        const color = ({
          todo: '',
          processing: 'blue',
          urgent: 'red',
          doing: 'gold',
        })[newNotice.status];
        newNotice.extra = <Tag color={color} style={{ marginRight: 0 }}>{newNotice.extra}</Tag>;
      }
      return newNotice;
    });
    return groupBy(newNotices, 'type');
  }

  handleNoticeClear = (type) => {
    message.success(`清空了${type}`);
    this.props.dispatch({
      type: 'app/clearNotices',
      payload: type,
    });
  }

  handleNoticeVisibleChange = (visible) => {
    if(visible) {
      this.props.dispatch({
        type: 'app/fetchNotices',
      });
    }
  }

  onMenuClick = ({key}) => {
    if('logout' == key) {
      this.props.dispatch({type:'app/logout'})
    }
  }

  render () {
    const { notifyCount, fetchingNotices, user, dispatch} = this.props;
    const noticeData = this.getNoticeData();
    const menu = (
      <Menu className={styles.menu} selectedKeys={[]} onClick={this.onMenuClick}>
        <Menu.Item disabled><Icon type="user" /> 个人中心</Menu.Item>
        <Menu.Item key="setting"><Icon type="setting" /> 个人设置</Menu.Item>
        <Menu.Item key="theme"><Icon type="wallet" /> 主题设置</Menu.Item>
        <Menu.Item><Icon type="copy" /><Feedback dispatch={dispatch}> 意见反馈</Feedback></Menu.Item>
        <Menu.Divider />
        <Menu.Item key="logout"><Icon type="logout" /> 退出登录</Menu.Item>
      </Menu>
    );

    return (
      <div className={styles.right}>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['2']}
          style={{ lineHeight: '56px',background:'#4397e6' }}
        >
          <Menu.Item key="1">
          <a href="http://doc.loozb.com/" target="_blank"><Icon type="file-ppt" />API文档</a>
          </Menu.Item>
          {/* <Menu.Item key="1">
            <HeaderSearch
            className={styles.search}
            placeholder="站内搜索"
            dataSource={['搜索提示一', '搜索提示二', '搜索提示三']}
            onSearch={(value) => {
              console.log('input', value); // eslint-disable-line
            }}
            onPressEnter={(value) => {
              console.log('enter', value); // eslint-disable-line
            }}
          />
          </Menu.Item> */}

          <Menu.Item key="3">
            {user.username ? (
            <Dropdown overlay={menu}>
              <span className={`${styles.action} ${styles.account}`}>
                欢迎您！{user.name}<Icon type="down" />
                <Avatar size="default" className={styles.avatar} src={user.avatar} />
              </span>
            </Dropdown>
          ) : <Spin size="small" style={{ marginLeft: 8 }} />}
          </Menu.Item>
          <Menu.Item key="2">
            <NoticeIcon
              className={styles.action}
              count={notifyCount}
              onItemClick={(item, tabProps) => {
                console.log(item, tabProps); // eslint-disable-line
              }}
              onClear={this.handleNoticeClear}
              onPopupVisibleChange={this.handleNoticeVisibleChange}
              loading={fetchingNotices}
              popupAlign={{ offset: [20, -16] }}
              >
              <NoticeIcon.Tab
                list={noticeData['通知']}
                title="通知"
                emptyText="你已查看所有通知"
                emptyImage="https://gw.alipayobjects.com/zos/rmsportal/wAhyIChODzsoKIOBHcBk.svg"
              />
              <NoticeIcon.Tab
                list={noticeData['消息']}
                title="消息"
                emptyText="您已读完所有消息"
                emptyImage="https://gw.alipayobjects.com/zos/rmsportal/sAuJeJzSKbUmHfBQRzmZ.svg"
              />
              <NoticeIcon.Tab
                list={noticeData['待办']}
                title="待办"
                emptyText="你已完成所有待办"
                emptyImage="https://gw.alipayobjects.com/zos/rmsportal/HsIsxMZiWKrNUavQUXqx.svg"
              />
          </NoticeIcon>
          </Menu.Item>
        </Menu>
      </div>
    )
  }
}
