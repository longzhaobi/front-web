import React from 'react';
import { routerRedux } from 'dva/router';
import { Menu, Icon, Avatar, Dropdown, Tag, message, Spin } from 'antd';
import moment from 'moment';
import groupBy from 'lodash/groupBy';
import styles from './Header.less'
import HeaderSearch from '@/components/HeaderSearch';
import NoticeIcon from '@/components/NoticeIcon';
import Feedback from '../Feedback';
// import RightSidebar from '@/components/Sidebar';
import RightHeader from '../Common/RightHeader';

import { Link, Route, Redirect, Switch } from 'dva/router';

export default class Header extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
    }
  }

  
  render () {

    return (
      <div className={styles.header}>
        {/* <RightSidebar collapse={this.state.collapse} togglerContent={this.togglerContent}/> */}
        <div className={styles.left}>
          <Link to="/">
            <img src="https://gw.alipayobjects.com/zos/rmsportal/iwWyPinUoseUxIAeElSx.svg" alt="logo" />
            <span style={{fontSize:20, color:'#fff',marginLeft:10}}>中后台项目基础后台模版</span>
          </Link>
        </div>
        <RightHeader {...this.props}/>
      </div>
    )
  }

}
