import React from 'react';
import { Menu, Icon} from 'antd';
import { Link, Route, Redirect, Switch } from 'dva/router';
import classnames from 'classnames';
import Debounce from 'lodash-decorators/debounce';
import styles from './Sidebar.less';
const { SubMenu } = Menu;
export default class Sidebar extends React.Component {
    constructor(props) {
      super(props)
      // 把一级 Layout 的 children 作为菜单项
      //上一次调用回调返回的值，或者是提供的初始值（initialValue）
      // const hasMenus = props.navData.reduce((arr, current) => arr.concat(current.children), []);
      this.menus = this.props.menuData;
      this.permission = localStorage.permission ?  localStorage.permission.split(",") : [];
      this.state = {
        openKeys: this.getDefaultCollapsedSubMenus(props),
      }
    }

    toggleCollapsed = () => {
      const {dispatch, menuStyle } = this.props;
      dispatch({type:'app/switchClick', payload: menuStyle === 'max' ? 'min' : 'max'});
      this.triggerResizeEvent();
    }

    @Debounce(600)
    triggerResizeEvent() { // eslint-disable-line
      const event = document.createEvent('HTMLEvents');
      event.initEvent('resize', true, false);
      window.dispatchEvent(event);
    }

    getDefaultCollapsedSubMenus = (props) => {
      const { location: { pathname } } = props || this.props;
      const snippets = pathname.split('/').slice(1, -1);
      const currentPathSnippets = snippets.map((item, index) => {
        const arr = snippets.filter((_, i) => i <= index);
        return arr.join('/');
      });
      let currentMenuSelectedKeys = [];
      currentPathSnippets.forEach((item) => {
        currentMenuSelectedKeys = currentMenuSelectedKeys.concat(this.getSelectedMenuKeys(item));
      });
      if (currentMenuSelectedKeys.length === 0) {
        return ['dashboard'];
      }
      return currentMenuSelectedKeys;
    }

    //返回false不需要渲染
    shouldComponentUpdate(nextProps, nextState) {
      const { location: { pathname }} = nextProps;
      return pathname !== '/home';
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.location.pathname !== this.props.location.pathname) {
        this.setState({
          openKeys: this.getDefaultCollapsedSubMenus(nextProps),
        });
      }
    }

    getSelectedMenuKeys = (path) => {

      if(path.indexOf('-refresh') > -1) {
        //如果是刷新页面
        const paths = path.split('-');
        path = paths[0];
      }

      const flatMenuKeys = this.getFlatMenuKeys(this.menus);
      if (flatMenuKeys.indexOf(path.replace(/^\//, '')) > -1) {
        return [path.replace(/^\//, '')];
      }
      if (flatMenuKeys.indexOf(path.replace(/^\//, '').replace(/\/$/, '')) > -1) {
        return [path.replace(/^\//, '').replace(/\/$/, '')];
      }
      return flatMenuKeys.filter((item) => {
        const itemRegExpStr = `^${item.replace(/:[\w-]+/g, '[\\w-]+')}$`;
        const itemRegExp = new RegExp(itemRegExpStr);
        return itemRegExp.test(path.replace(/^\//, '').replace(/\/$/, ''));
      });
    }

    getFlatMenuKeys(menus) {
      let keys = [];
      menus.forEach((item) => {
        if (item.children) {
          keys.push(item.path);
          keys = keys.concat(this.getFlatMenuKeys(item.children));
        } else {
          keys.push(item.path);
        }
      });
      return keys;
    }

    getCurrentMenuSelectedKeys = (props) => {
      const { location: { pathname } } = props || this.props;
      const keys = pathname.split('/').slice(1);
      if (keys.length === 1 && keys[0] === '') {
        return [this.menus[0].key];
      }
      return keys;
    }

    isAuth = (perm) => {

      let flag = false;
      for (let p of this.permission) {
        if(p === perm + ':view') {
          flag = true;
          break;
        }
      }
      return flag;
    }

    getMenuItems = (menusData, pathname = '') => {
      if(pathname === '/home') {
        return;
      }
      return menusData.map(item => {
        if(item.children && pathname.indexOf(item.path) === 1) {
          return this.getNavMenuItems(item.children)
        }
      })
    }

    getNavMenuItems = (menusData, parentPath = '') => {
      if (!menusData) {
        return [];
      }
      return menusData.map((item) => {
        if (!item.path || item.hide) {
          //如果没有name或者为隐藏路由，则不显示
          return null;
        }
        //如果路由对象有name属性，则进行权限判断，否则放行
        if(item.key) {
          if(!this.isAuth(item.key)) {
            return null;
          }
        }
        let itemPath;
        if (item.path.indexOf('http') === 0) {
          itemPath = item.path;
        } else {
          itemPath = `/${item.path || ''}`.replace(/\/+/g, '/');
        }
        if (item.children && item.children.some(child => child.name)) {
          return (
            <SubMenu
              title={
                item.icon ? (
                  <span>
                    <Icon type={item.icon} />
                    <span>{item.name}</span>
                  </span>
                ) : item.name
              }
              key={item.path}
            >
              {this.getNavMenuItems(item.children, itemPath)}
            </SubMenu>
          );
        }
        const icon = item.icon && <Icon type={item.icon} />;
        return (
          <Menu.Item key={item.path}>
            {
              /^https?:\/\//.test(itemPath) ? (
                <a href={itemPath} target={item.target}>
                  {icon}<span>{item.name}</span>
                </a>
              ) : (
                <Link
                  to={itemPath}
                  target={item.target}
                  replace={itemPath === this.props.location.pathname}
                >
                  {icon}<span>{item.name}</span>
                </Link>
              )
            }
          </Menu.Item>
        );
      });
    }

    handleOpenChange = (openKeys) => {
      const lastOpenKey = openKeys[openKeys.length - 1];
      const isMainMenu = this.menus.some(
        item => lastOpenKey && (item.key === lastOpenKey || item.path === lastOpenKey)
      );
      this.setState({
        openKeys: isMainMenu ? [lastOpenKey] : [...openKeys],
      });
    }

    componentWillUnmount() {
      this.triggerResizeEvent.cancel();
    }

    render () {
      const { app, menuStyle, menuData, location: { pathname } } = this.props;
      const { openKeys } = this.state;
      const collapsed = menuStyle === 'max';
      const menuProps = !collapsed ? {} : {
        openKeys
      };
      // if pathname can't match, use the nearest parent's key
      let selectedKeys = this.getSelectedMenuKeys(pathname);
      if (!selectedKeys.length) {
        selectedKeys = [openKeys[openKeys.length - 1]];
      }
      const cls = classnames({
        [styles.Sidebar]: true,
        [styles.max]: menuStyle === 'max',
        [styles.min]: menuStyle === 'min'
      });
      return (
        <div className={cls} style={{height: 'calc(100vh - 60px)'}}>
          <div className={styles.switchBar}  onClick={this.toggleCollapsed}>
          <Icon type={collapsed ? 'menu-fold':'menu-unfold'} />
          </div>
          <Menu
            theme="dark"
            mode="inline"
            {...menuProps}
            className="loozb-menu-style"
            onOpenChange={this.handleOpenChange}
            selectedKeys={selectedKeys}
            style={{ width: '100%'}}
            inlineCollapsed={!collapsed}
          >
            {this.getMenuItems(menuData, pathname)}
          </Menu>
        </div>
      )
    }

}
