import React from 'react';
import { routerRedux } from 'dva/router';
import { Menu, Icon } from 'antd';
import styles from './Header.less'

import RightHeader from '../Common/RightHeader';

import { Link, Route, Redirect, Switch } from 'dva/router';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.permission = localStorage.permission ?  localStorage.permission.split(",") : [];
    this.state = {
      collapse: false,
    }
  }

  onMenuClick = ({key}) => {
    if (key === 'logout') {
      this.props.dispatch({ type: 'app/logout' });
    } else if(key === 'setting') {
      this.props.dispatch(routerRedux.push("/account/settings"));
    } else if(key === 'theme') {
      // this.togglerContent();
    }
  }

  

  togglerContent = () => {
    this.setState({
      collapse: !this.state.collapse,
    })
  }

  isAuth = (perm) => {

    let flag = false;
    for (let p of this.permission) {
      if(p === perm + ':view') {
        flag = true;
        break;
      }
    }
    return flag;
  }

  shouldComponentUpdate (nextProps, nextState) {
    return true;
  }

  getTopMenu = menus => menus.map(item => {
    if (!item.path || item.hide) {
      //如果没有name或者为隐藏路由，则不显示
      return null;
    }

    //如果路由对象有name属性，则进行权限判断，否则放行
    if(item.key) {
      if(!this.isAuth(item.key)) {
        return null;
      }
    }
    let itemPath;
    if (item.path.indexOf('http') === 0) {
      itemPath = item.path;
    } else {
      itemPath = `/${item.path || ''}`.replace(/\/+/g, '/');
    }
    const icon = item.icon && <Icon type={item.icon} />;
    return (
      <Menu.Item key={item.path}>
        {
          /^https?:\/\//.test(itemPath) ? (
            <a href={itemPath} target={item.target}>
              {icon}<span>{item.name}</span>
            </a>
          ) : (
            <Link
              to={itemPath}
              target={item.target}
              replace={itemPath === this.props.location.pathname}
            >
              {icon}<span>{item.name}</span>
            </Link>
          )
        }
      </Menu.Item>
    )
  })

  render () {
    const { menuData, location:{pathname}} = this.props;
    const arr = pathname.split('/');
    return (
      <div className={styles.header}>
        {/* <RightSidebar collapse={this.state.collapse} togglerContent={this.togglerContent}/> */}
        <div className={styles.left}>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={arr}
            selectedKeys={arr}
            style={{ lineHeight: '56px',background:'#4397e6' }}
          >
            <Menu.Item key="logo">
              <Link to="/">
                <img src="https://gw.alipayobjects.com/zos/rmsportal/iwWyPinUoseUxIAeElSx.svg" alt="logo" />
                <span style={{fontSize:20, color:'#fff',marginLeft:10}}>中后台项目基础后台模版</span>
              </Link>
            </Menu.Item>
            {
              this.getTopMenu(menuData)
            }
          </Menu>
        </div>
        <RightHeader {...this.props}/>
      </div>
    )
  }

}
