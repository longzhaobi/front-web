import React from 'react';
import {Menu} from 'antd';
import classnames from 'classnames';
import styles from './Content.less'

export default class Content extends React.PureComponent {
  constructor(props) {
    super(props)
  }

  render () {
    const {children, menuStyle} = this.props;

    const cls = classnames({
      [styles.content]: true,
      [styles.max]: menuStyle === 'max',
      [styles.min]: menuStyle === 'min'
    });
    return (
        <div className={cls} style={{height: 'calc(100vh - 60px)'}}>
            {children}
        </div>
    )
  }

}
