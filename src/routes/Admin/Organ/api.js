import request from '@/utils/axios';
import qs from 'qs';
export async function fetch(params) {
  return request({
    url:'/api/organs/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/organs',
    method:'post',
    data:params
  });
}

export async function update(params) {
  return request({
    url:`/api/organs`,
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:'/api/organs',
    method:'delete',
    data: params
  });
}

export async function fetchPermission() {
  return request({
    url:'/api/permissions/init'
  });
}
