import React, { Component } from 'react';
import { Form, Input, Modal, Button, InputNumber, Col, message, Icon, Select } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
import WithModal from '@/hocs/WithModal'

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

@Form.create()
@WithModal({pathname:'organ/fetch'})
export default class FormModal extends Component {

  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
    this.state = {
      visible: false,
      permission: [],
      loading: false
    };
  }


  submitHandler = () => {
    const { item, option } = this.props;
    this.props.form.validateFields((err, params) => {
      if (!err) {
        const formData = option === 'create' ? { ...params, pid: item.id, pids: item.pids + item.id + '/' } : { ...params };
        this.props.okHandler(formData)
      }
    });
  };

  render() {
    const { children, title, record, loading, visible, showModelHandler, hideModelHandler } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <span>
        <span onClick={showModelHandler}>
          {children}
        </span>
        <Modal
          title={title}
          visible={visible}
          width={560}
          maskClosable={false}
          onCancel={hideModelHandler}
          footer={[
            <Button key="back" type="ghost" size="large" onClick={hideModelHandler}>取消</Button>,
            <Button key="submit" type="primary" size="large" disabled={loading} loading={loading} onClick={this.submitHandler}>
              {loading ? '处理中...' : '确定'}
            </Button>,
          ]}
        >
          <Form layout='horizontal' onSubmit={this.submitHandler}>
            <FormItem
              {...formItemLayout}
              label="组织名称"
            >
              {getFieldDecorator('name', {
                rules: [
                  { required: true, max: 25, message: '组织名称不能为空，且不能超过25位字符' }
                ],
                initialValue: record['name'] || ''
              })(
                <Input type="text" placeholder="请输入组织名称" />
                )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="权重（排序）"
            >
              {getFieldDecorator('weight', {
                initialValue: record['weight'] || 0
              })(
                <InputNumber min={0} />
                )}
            </FormItem>

            <FormItem
              {...formItemLayout}
              label="描述（选填）"
            >
              {getFieldDecorator('url', {
                rules: [
                  { max: 200, message: '组织不能超过 200 位字符' }
                ],
                initialValue: record['description'] || ''
              })(
                <Input type="text" placeholder="请输入组织描述信息" />
                )}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}
