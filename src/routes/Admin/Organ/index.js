import React, {PropTypes, PureComponent} from 'react';
import {connect} from 'dva';

import {Table, Select, Input, Button, Row, Col, Popconfirm, Icon, Divider} from 'antd';
const Search = Input.Search;

import columns from './columns';

import FormModal from './FormModal';
import styles from './index.css';

import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

@connect(({organ, loading}) => ({
  ...organ,
  namespace: 'organ',
  loading,
}))
@WithList({pathname: 'organ/fetch'})
export default class Organ extends PureComponent {

  render() {
    const {data, current, fetching, total, size, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection} = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    function title() {
      return (
        <Row>
          <Col span={16}>
            <FormModal  record={{}} loading={loading} item={{id:1, pids:'0/'}} dispatch={dispatch} namespace={namespace} option='create' title="新增资源">
              <IButton type="primary" icon="plus" perm="organ:create">新增根节点</IButton>
            </FormModal>
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
              <IButton type="danger" disabled={!hasSelected} icon="delete" perm="organ:remove">删除</IButton>
            </Popconfirm>
            <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <div>
        <FormModal record={{}} loading={loading} item={record} dispatch={dispatch} namespace={namespace} option='create' title="新增资源">
          <IButton perm="organ:remove" a>  新增 </IButton>
        </FormModal>
        <FormModal record={record} loading={loading} dispatch={dispatch} namespace={namespace} option='update' title="编辑资源">
          <IButton perm="organ:update" a> <Divider type="verticle"/> 编辑 </IButton>
        </FormModal>
        <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({id:record.id})}>
          <IButton perm="organ:remove" a> <Divider type="verticle"/> 删除 </IButton>
        </Popconfirm>
      </div>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        size="middle"
        scroll={{ y: table_height + 50 }}
        bordered
        rowKey="id"
        loading={loading.effects['organ/fetch']}
        title={() => title()}
      />
    )
  }

}
