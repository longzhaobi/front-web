const columns = (toolBar) => {
  return [{
    title: '机构名称',
    dataIndex: 'name',
    width: 240,
  }, {
    title: '描述信息',
    dataIndex: 'identity',
  }, {
    title: '创建日期',
    dataIndex: 'createTime',
    width: 180,
  }, {
    title: '维护日期',
    dataIndex: 'updateTime',
    width: 180,
  }, {
    title: '操作',
    key: 'operation',
    width: 180,
    render: (text, record, index) => toolBar(text, record, index)
  }]
}

export default columns;
