import React, {PropTypes, PureComponent} from 'react';
import {connect} from 'dva';
import {Table, Select, Input, Button, Row, Col, Popconfirm, Icon, Divider} from 'antd';
const Search = Input.Search;

import columns from './columns';

import FormModal from './FormModal';
import styles from './index.css';

import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

@connect(({resource, loading}) => ({
  ...resource,
  namespace: 'resource',
  loading,
}))
@WithList({pathname: 'resource/fetch'})
export default class Resource extends PureComponent {

  render () {
    const {data, fetching, current, total, size, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection} = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    function title() {
      return (
        <Row>
          <Col span={16}>
            <FormModal  record={{}} item={{id:1, pids:'0/'}} dispatch={dispatch} namespace={namespace} option='create' loading={loading} title="新增资源">
              <IButton type="primary" icon="plus" perm="resource:create">新增根节点</IButton>
            </FormModal>
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
              <IButton type="danger" disabled={!hasSelected} icon="delete" perm="resource:remove">删除</IButton>
            </Popconfirm>
            <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <div>
        <FormModal record={{}} item={record} dispatch={dispatch} namespace={namespace} option='create' title="新增资源">
          <IButton perm="resource:remove" a>  新增 </IButton>
        </FormModal>
        <FormModal record={record} dispatch={dispatch} namespace={namespace} option='update' title="编辑资源">
          <IButton perm="resource:update" a> <Divider type="verticle"/> 编辑 </IButton>
        </FormModal>
        <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({id:record.id})}>
          <IButton perm="resource:remove" a> <Divider type="verticle"/> 删除 </IButton>
        </Popconfirm>
      </div>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        size="middle"
        scroll={{ y: table_height + 50, x: 1530 }}
        bordered
        rowKey="id"
        loading={loading.effects['resource/fetch']}
        title={() => title()}
      />
    )
  }
}
