import request from '@/utils/axios';
import qs from 'qs';
export async function fetch(params) {
  return request({
    url:'/api/resources/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/resources',
    method:'post',
    data:params
  });
}

export async function update(params) {
  return request({
    url:`/api/resources`,
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:'/api/resources',
    method:'delete',
    data: params
  });
}

export async function fetchPermission() {
  return request({
    url:'/api/permissions/init'
  });
}
