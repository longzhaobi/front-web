import React, { Component } from 'react';
import { Form, Input, Modal, Button, Radio, message, Checkbox, Icon, Row, Col} from 'antd';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
import WithModal from '@/hocs/WithModal'

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

@Form.create()
@WithModal({pathname:'dic/fetch'})
class FormModal extends Component {

  constructor(props) {
    super(props);
  }

  onSubmit = () => {
    this.props.form.validateFields((err, params) => {
      if (!err) {
        this.props.okHandler(params);
      }
    })
  }

  render() {
    const { children, title, loading, record, showModelHandler, hideModelHandler, visible } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <span>
        <span onClick={showModelHandler}>
          { children }
        </span>
        <Modal
          title={title}
          visible={visible}
          width={560}
          maskClosable={false}
          onCancel={hideModelHandler}
          footer={[
          <Button key="back" type="ghost" size="large" onClick={hideModelHandler}>取消</Button>,
          <Button key="submit" type="primary" size="large" disabled={loading} loading={loading} onClick={this.onSubmit}>
            {loading ? '处理中...' : '确定'}
          </Button>,
        ]}
        >
          <Form layout='horizontal' onSubmit={this.onSubmit}>
            <FormItem
              {...formItemLayout}
              label="名称"
            >
            {getFieldDecorator('name', {
              rules: [
                  { required: true, max: 25, message: '名称不能大于 25 个字符' }
                ],
                initialValue: record['name'] || ''
              })(
              <Input type="text" placeholder="请输入角色名称" />
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="编码"
            >
            {getFieldDecorator('code', {
              rules: [
                  { required: true, max: 25, message: '标识不能为空且不能小于 25 个字符' }
                ],
                initialValue: record['code'] || ''
              })(
              <Input type="text" placeholder="请输入角色名称" />
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="值"
            >
              {getFieldDecorator('value', {
                rules: [
                  { required: true, max: 100, message: '角色描述不能超过 100 个字符' },
                ],
                initialValue: record['value'] || ''
              })(
                <Input type="text" placeholder="请输入值" />
              )}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}

export default Form.create()(FormModal);
