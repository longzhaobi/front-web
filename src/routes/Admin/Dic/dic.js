import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

export default modelExtend(grid(service, '/admin/dic', 'dic'), {
  namespace: 'dic',

  state: {
    namespace: 'dic'
  },

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const dic = yield select(({ dic }) => dic);
      const params = {
        current: dic.current,
        size: dic.size,
        keyword: dic.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    },
  }
});
