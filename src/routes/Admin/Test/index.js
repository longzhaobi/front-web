import React, { PropTypes, PureComponent } from 'react';
import {connect} from 'dva';

import Markdown from '@/components/Markdown';
@connect(({app}) => ({
  ...app
}))
export default class Test extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      content:''
    }
  }

  render() {
    const v = `## 只求极致
    [ **M** ] arkdown + E [ **ditor** ] = **Mditor**
    > Mditor 是一个简洁、易于集成、方便扩展、期望舒服的编写 markdown 的编辑器，仅此而已...`;
    return (
      <Markdown defaultValue={v} onChange = {(value) => this.setState({content:value})}/>
    )
  }
}
