import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

import {message} from 'antd';
export default modelExtend(grid(service, '/admin/file', 'file'), {
  namespace: 'file',

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const o = yield select(({ file }) => file);
      const params = {
        current: o.current,
        size: o.size,
        keyword: o.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    },

  }
});
