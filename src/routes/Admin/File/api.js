import request from '@/utils/axios';
import qs from 'qs';
export async function fetch(params) {
  return request({
    url:'/api/files/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/files',
    method:'post',
    data:qs.stringify(params)
  });
}

export async function update(params) {
  return request({
    url:`/api/files`,
    method:'put',
    data:qs.stringify(params)
  });
}

export async function remove(params) {
  return request({
    url:'/api/files',
    method:'delete',
    data: params
  });
}
