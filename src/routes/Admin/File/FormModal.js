import React, { Component } from 'react';
import { Form, Input, Modal, Button, Radio, message, Checkbox, Icon, Row, Col} from 'antd';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

import WithModal from '@/hocs/WithModal'

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

@Form.create()
@WithModal({pathname: 'permission/fetch'})
export default class FormModal extends Component {

  constructor(props) {
    super(props);
  }

  onSubmit = () => {
    this.props.form.validateFields((err, params) => {
      if (!err) {
        this.props.okHandler(params);
      }
    })
  }

  render() {
    const { children, title, loading, record, visible, showModelHandler, hideModelHandler } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <span>
        <span onClick={showModelHandler}>
          { children }
        </span>
        <Modal
          title={title}
          visible={visible}
          width={560}
          maskClosable={false}
          onCancel={hideModelHandler}
          footer={[
          <Button key="back" type="ghost" size="large" onClick={hideModelHandler}>取消</Button>,
          <Button key="submit" type="primary" size="large" disabled={loading} loading={loading} onClick={this.onSubmit}>
            {loading ? '处理中...' : '确定'}
          </Button>,
        ]}
        >
          <Form layout='horizontal' onSubmit={this.onSubmit}>
            <FormItem
              {...formItemLayout}
              label="权限名称"
            >
            {getFieldDecorator('name', {
              rules: [
                 { required: true, max: 25, message: '权限名称不能超过25位字符' }
               ],
               initialValue: record['name'] || ''
             })(
              <Input type="text" placeholder="请输入权限名称" />
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="权限标识"
            >
              {getFieldDecorator('permission', {
                rules: [
                   { required: true, max: 25, message: '权限名称不能超过25位字符' }
                 ],
                 initialValue: record['permission'] || ''
               })(
                <Input type="text" placeholder="请输入权限标识" />
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="权限描述"
              help="权限描述，可以为空"
            >
              {getFieldDecorator('description', {
                rules: [
                   { max: 200, message: '权限名称不能超过200位字符' }
                 ],
                 initialValue: record['description'] || ''
               })(
                <Input type="textarea" placeholder="请输入权限描述" />
              )}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}
