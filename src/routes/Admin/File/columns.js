const columns = (toolBar) => {
    return [{
      title: '#',
      width: 50,
      fixed:'left',
      render: (text, record, index) => (
          <span>{index + 1}</span>
      )
    }, {
      title: '附件编号',
      dataIndex: 'id',
      width: 200
    }, {
      title: '附件名称',
      dataIndex: 'fileName',
      width: 180
    }, {
      title: '附件地址',
      dataIndex: 'filePath',
    }, {
      title: '附件类型',
      dataIndex: 'fileType',
      width: 180,
    }, {
      title: '附件模块',
      dataIndex: 'fileModule',
      width: 180,
    }, {
      title: '创建日期',
      dataIndex: 'createTime',
      width: 180
    }, {
      title: '维护日期',
      dataIndex: 'updateTime',
      width: 180
    }, {
      title: '操作',
      key: 'operation',
      width: 150,
      fixed:'right',
      render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
