import React, { PureComponent } from 'react';
import {connect} from 'dva';
import { Table, Select, Input, Row, Col, Popconfirm } from 'antd';
const Option = Select.Option;
const Search = Input.Search;

import FormModal from './FormModal';
import styles from './index.css';

import columns from './columns';

import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

@connect(({cache, loading}) => ({
  ...cache,
  namespace: 'cache',
  loading,
}))
@WithList({pathname: 'cache/fetch'})
export default class Cache extends PureComponent {

  render () {

    const { data, table, loading, tableName, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection } = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    function handleChange(tableName) {
      dispatch({type: 'cache/fetch', payload: {tableName}});
    }

    const children = [];
    for (let i = 0; i < table.length; i++) {
      children.push(<Option key={table[i].key}>{table[i].value}</Option>);
    }

    function title() {
      return (
        <Row>
          <Col span={12}>
            <FormModal dispatch={dispatch} namespace={namespace} loading={loading}>
              <IButton type="primary" icon="fork" perm={`${namespace}:create`}>高级操作</IButton>
            </FormModal>
            <Popconfirm title="确定要删除吗？" onConfirm={() => dispatch({type:`${namespace}/removeCache`, payload:'LOOZB:'})}>
              <IButton type="danger" icon="delete" perm="cache:remove">清除所有缓存</IButton>
            </Popconfirm>
            <Popconfirm title="确定要删除吗？" onConfirm={() => dispatch({type:`${namespace}/removeCurrentCache`, payload: tableName})}>
              <IButton type="danger" icon="delete" perm="cache:remove">清除当前缓存</IButton>
            </Popconfirm>
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
              <IButton type="danger" disabled={!hasSelected} icon="delete" perm="permission:remove">清除选择缓存</IButton>
            </Popconfirm>
            <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={12} style={{ float: 'right' }} >
            <Search size="default" style={{ width: 300, float: 'right' }} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
            <Select
              showSearch
              style={{ width: 180, float: 'right', marginRight: '8px' }}
              placeholder="请选择表名"
              optionFilterProp="children"
              onChange={handleChange}
              defaultValue={tableName}
              size="default"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            >
              {children}
            </Select>
          </Col>
        </Row>
      )
    }

    const toolBar = (text, record, index) => (
      <div>
        <Popconfirm title="确定要清除缓存吗？" onConfirm={() => removeHandler({ id: record.cacheKey })}>
          <IButton perm="cache:remove" a> 清除缓存 </IButton>
        </Popconfirm>
      </div>
    )
    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        size="middle"
        scroll={{ y: table_height }}
        bordered
        rowKey="cacheKey"
        loading={loading.effects['cache/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )
  }
}
