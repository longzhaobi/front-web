import request from '@/utils/axios';
import qs from 'qs';
export async function fetch(params) {
  return request({
    url:'/api/caches/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/caches',
    method:'post',
    data:params
  });
}

export async function update(params) {
  return request({
    url:`/api/caches`,
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:'/api/caches',
    method:'delete',
    data: params
  });
}

export function removeCache(params) {
  return request({
    url:'/api/caches/removeCache',
    method:'post',
    data: params
  });
}

export function removeCurrentCache(params) {
  return request({
    url:'/api/caches/removeCurrentCache',
    method:'post',
    data: params
  });
}
