import React from 'react';
import { routerRedux, Route, Switch } from 'dva/router';
import { getRoutes } from '../../utils/utils';
export default class Admin extends React.PureComponent {
  constructor(props) {
    super(props)
  }

  render() {
    const { match, routerData, location } = this.props;
    const routes = getRoutes(match.path, routerData);
    return (
      <div>
        <Switch>
          {
            routes.map(item =>
              (
                <Route
                  key={item.key}
                  path={item.path}
                  component={item.component}
                  exact={item.exact}
                />
              )
            )
          }
        </Switch>
      </div>
    )
  }
}