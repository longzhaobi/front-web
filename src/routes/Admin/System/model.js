import * as service from './api';
export default {

  namespace: 'system',

  state: {

  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    *queryUsageRate({ payload }, { call }) {
      return yield call(service.queryUsageRate, payload);
    },
    *queryTestInfo({payload}, {call}) {
      return yield call(service.queryTestInfo, payload);
    }
  },

  reducers: {
  },

};
