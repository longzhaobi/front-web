import { Badge } from 'antd';
const columns = (toolBar) => {
    return [{
        title: '序号',
        width: 50,
        render: (text, record, index) => (
            <span>{index + 1}</span>
        )
    }, {
        title: '检测名称',
        dataIndex: 'testName',
        width: 200,
    }, {
        title: '检测项',
        dataIndex: 'testItem',
        width:200,
    }, {
        title: '检测信息',
        dataIndex: 'testInfo',
    }]
}

export default columns;
