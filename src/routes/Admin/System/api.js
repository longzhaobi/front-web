import request from '@/utils/axios';
export async function queryUsageRate(params) {
  return request({
    url:'/api/system/usageRate',
    params
  });
}

export async function queryTestInfo(params) {
  return request({
    url:'/api/system/testInfo',
    params
  });
}
