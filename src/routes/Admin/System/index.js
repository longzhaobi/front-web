import React from 'react';
import {connect} from 'dva';
import { Card, Progress, Table } from 'antd';
import columns from './columns';
import styles from './index.less';

@connect(({system, loading, app}) => ({
  ...system,
  namespace: 'system',
  loading,
}))
export default class System extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      cup:0,
      jvm:0,
      memory:0,
      data:[],
    }
  }

  componentDidMount () {
    //获取仪表盘数据
    this.queryUsageRate();


    //获取检测信息数据
    this.queryTestInfo();

    //定时刷新
    // this.interval = setInterval(this.queryUsageRate, 3000);
  }

  queryUsageRate = () => {
    this.setState({loading:true});
    this.props.dispatch({type: 'system/queryUsageRate'}).then(data => {
      if(data) {
        this.setState({...data.data, loading: false});
      }
    })
  }

  queryTestInfo = () => {
    this.props.dispatch({type: 'system/queryTestInfo'}).then(data => {
      if(data) {
        this.setState({data:data.data, loading: false});
      }
    })
  }

  componentWillUnmount() {
    // clearInterval(this.interval);
  }

  render() {

    const gridStyle = {
      width: '25%',
      textAlign: 'center',
    };


    const { loading, jvm, cpu, memory, data } = this.state;

    return (
      <div className={styles['system-box']}>
        <Card title="资源监视" extra={<a href="#">More</a>} >
          <Card.Grid style={gridStyle}>
            <Progress type="dashboard" percent={cpu} format={percent => `CPU使用率 ${percent}%`} width={180} strokeWidth = {4}/>
          </Card.Grid>
          <Card.Grid style={gridStyle}>
            <Progress type="dashboard" percent={jvm} status="exception" format={percent => `JVM使用率 ${percent}%`} width={180} strokeWidth = {4}/>
          </Card.Grid>
          <Card.Grid style={gridStyle}>
            <Progress type="dashboard" percent={memory} format={percent => `Memory使用率 ${percent}%`} width={180} strokeWidth = {4}/>
          </Card.Grid>
          <Card.Grid style={gridStyle}>
            <Progress type="dashboard" percent={memory} format={percent => `Disk使用率 ${percent}%`} width={180} strokeWidth = {4}/>
          </Card.Grid>
        </Card>
        <Card title="检测信息" extra={<a href="#">More</a>}>
          <Table columns={columns()} rowKey="testItem" dataSource={data} size="middle" bordered />
        </Card>
      </div>
    )
  }
}
