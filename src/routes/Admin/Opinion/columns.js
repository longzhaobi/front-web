const columns = (toolBar) => {
    return [{
        title: '标题',
        dataIndex: 'title'
    }, {
        title: '联系方式',
        dataIndex: 'contactWay',
        width: 180,
    }, {
        title: '创建日期',
        dataIndex: 'createTime',
        width: 180
    }, {
        title: '操作',
        key: 'operation',
        width: 150,
        render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
