import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

import {message} from 'antd';
export default modelExtend(grid(service, '/admin/opinion', 'opinion'), {
  namespace: 'opinion',

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const o = yield select(({ opinion }) => opinion);
      const params = {
        current: o.current,
        size: o.size,
        keyword: o.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    },

  }
});
