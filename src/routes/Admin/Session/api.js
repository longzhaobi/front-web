import request from '@/utils/axios';
import qs from 'qs';
export async function fetch(params) {
  return request({
    url:'/api/session/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/session',
    method:'post',
    data:params
  });
}

export async function update(params) {
  return request({
    url:`/api/session`,
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:'/api/session',
    method:'delete',
    data: params
  });
}

export function removeAllOnline() {
  return request({
    url:`/api/session/removeAllOnline`,
    method:'delete'
  });
}
