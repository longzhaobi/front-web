import React, { PropTypes, PureComponent } from 'react';
import {connect} from 'dva';
import { Table, Input, Button, Row, Col, Popconfirm } from 'antd';
const Search = Input.Search;

import WithList from '@/hocs/WithList';
import columns from './columns';

@connect(({session, loading, app}) => ({
  ...session,
  user: app.user,
  namespace: 'session',
  loading,
}))
@WithList({pathname: 'session/fetch'})
export default class Session extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    const { data, current, total, size, loading, selectedRowKeys, dispatch, namespace, keyword, user, onSearch, onChange, page, rowSelection, fetching } = this.props;

    function removeOnlineHandler(record) {
      if (record.online == '0') {
        message.warn("此用户已处于离线状态");
      } else {
        dispatch({
          type: `${namespace}/remove`,
          payload: { token: record.sessionId }
        })
      }

    }
    function title() {
      return (
        <Row>
          <Col span={16}>
            <Popconfirm title="确定要删除吗？" onConfirm={() => dispatch({type: `${namespace}/removeAllOnline`})}>
              <Button type="danger" size="default" icon="delete">全部下线</Button>
            </Popconfirm>
          </Col>
          <Col span={8} style={{ float: 'right' }} >
            <Search size="default" style={{ width: 300, float: 'right' }} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar = (text, record, index) => (
      <div>
        <Popconfirm title="确定要删除吗？" onConfirm={() => removeOnlineHandler(record)}>
          <a href="javascript:void(0)" disabled={record.userId === user.id || record.online == '0'}>强制下线</a>
        </Popconfirm>
      </div>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        scroll={{ y: table_height}}
        bordered
        rowKey="id"
        size={tableSize}
        loading={loading.effects['session/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )
  }
}
