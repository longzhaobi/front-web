import React, {PropTypes, PureComponent} from 'react';
import { connect } from 'dva';
import {Table, Select, Input, Button, Row, Col, Popconfirm, Icon, Divider} from 'antd';
const Option = Select.Option;
const Search = Input.Search;

import FormModal from './FormModal';
import AuthModal from './AuthModal';
import styles from './index.css';

import columns from './columns';


import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';


@connect(({role, loading}) => ({
  ...role,
  namespace: 'role',
  loading,
}))
@WithList({pathname: 'role/fetch'})
export default class Role extends PureComponent {

  constructor(props) {
    super(props);
  }

  render () {
    const {data, loading, fetching, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection } = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    function title() {
      return (
        <Row>
          <Col span={16}>
          <FormModal  record={{}} dispatch={dispatch} namespace={namespace} option='create' loading={loading} title="新增角色">
            <IButton type="primary" icon="plus" perm="role:create"> 新增 </IButton>
          </FormModal>
          <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
            <IButton type="danger" disabled={!hasSelected} perm="role:remove"  icon="delete">删除</IButton>
          </Popconfirm>
          <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入任务名称查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <div>
          <AuthModal record={record} dispatch={dispatch} namespace={namespace} loading={loading}>
            <IButton perm="role:allot" a> 授权 </IButton>
          </AuthModal>

          <FormModal record={record} dispatch={dispatch} namespace={namespace} option='update' loading={loading} title="编辑用户">
            <IButton perm="role:update" a> <Divider type="verticle"/>编辑 </IButton>
          </FormModal>

          <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({id:record.id})}>
            <IButton perm="role:remove" a>  <Divider type="verticle"/>删除 </IButton>
          </Popconfirm>
      </div>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        size="middle"
        scroll={{ y: table_height }}
        bordered
        rowKey="id"
        loading={loading.effects['role/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )
  }
}
