const columns = (toolBar) => {
    return [{
        title: '#',
        width: 50,
        render: (text, record, index) => (
            <span>{index + 1}</span>
        )
    }, {
        title: '角色名称',
        dataIndex: 'name',
        width: "180px"
    }, {
        title: '角色标识',
        dataIndex: 'role',
        width: 180,
    }, {
        title: '描述',
        dataIndex: 'description',
        // width:140,
    }, {
        title: '注册日期',
        dataIndex: 'createTime',
        width: 180
    }, {
        title: '维护日期',
        dataIndex: 'updateTime',
        width: 180
    }, {
        title: '操作',
        key: 'operation',
        width: 180,
        render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
