import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

import {message} from 'antd';
export default modelExtend(grid(service, '/admin/permission', 'permission'), {
  namespace: 'permission',

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const o = yield select(({ permission }) => permission);
      const params = {
        current: o.current,
        size: o.size,
        keyword: o.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    },

  }
});
