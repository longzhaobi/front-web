import React, { PropTypes, PureComponent } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import { Table, Select, Input, Button, Row, Col, Popconfirm, Dropdown, Icon, Divider, Menu } from 'antd';
const Option = Select.Option;
const Search = Input.Search;

import FormModal from './FormModal';
import NoticeModal from './NoticeModal';
import AuthModal from './AuthModal';
import styles from './index.css';
import columns from './columns';
import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';
import ImportModal from '@/components/ImportModal';
import ExportModal from '@/components/ExportModal';
@connect(({user, loading}) => ({
  ...user,
  namespace: 'user',
  loading,
}))
@WithList({pathname: 'user/fetch'})
export default class User extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {

    const { data, current, total, size, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection, fetching } = this.props;

    const hasSelected = selectedRowKeys.length > 0;

    function lockedHandler() {

    }

    const menu = (
      <Menu>
        <Menu.Item key="1"><Icon type="upload" /> <ImportModal title="Excel 用户数据导入" action="/api/users/excelUpload">数据导入</ImportModal></Menu.Item>
        <Menu.Item key="2"><Icon type="download" /> <ExportModal title="Excel 用户数据导出">数据导出</ExportModal></Menu.Item>
      </Menu>
    );

    function title() {
      return (
        <Row>
          <Col span={12}>
            <FormModal record={{}} dispatch={dispatch} namespace={namespace} option='create' loading={loading} title="新增用户">
              <IButton type="primary" icon="plus" perm="user:create"> 新增 </IButton>
            </FormModal>
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
              <IButton type="danger" disabled={!hasSelected} perm="user:remove" icon="delete">删除</IButton>
            </Popconfirm>
            <NoticeModal dispatch={dispatch} namespace={namespace} loading={loading}>
              <IButton type="primary" disabled={!hasSelected} perm="user:view" icon="bell"> 通知 </IButton>
            </NoticeModal>
            <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={12} style={{ textAlign: 'right' }} >
            <Dropdown overlay={menu}>
              <Button icon="ellipsis" style={{ marginRight: 8 }}>
                更多操作 <Icon type="down" />
              </Button>
            </Dropdown>
            <Search size="default" style={{ width: 300}} defaultValue={keyword} placeholder="输入任务名称查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar = (text, record, index) => (
      <div>
        <AuthModal record={record} dispatch={dispatch} namespace={namespace} loading={loading}>
          <IButton perm="user:allot" a> 授权 </IButton>
        </AuthModal>
        <FormModal loading={loading} record={record} dispatch={dispatch} namespace={namespace} option='update' title="编辑用户">
          <IButton perm="user:update" a> <Divider type="verticle" />编辑 </IButton>
        </FormModal>
        <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({ id: record.id })}>
          <IButton perm="user:remove" a> <Divider type="verticle" />删除 </IButton>
        </Popconfirm>
        <Popconfirm title="确定锁定吗？" onConfirm={() => lockedHandler(record.id, record.locked, record.idcard)}>
          <IButton perm="user:remove" a> <Divider type="verticle" />{record.locked === '1' ? '解锁' : '锁定'} </IButton>
        </Popconfirm>
      </div>
    )
    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        size="middle"
        scroll={{ y: table_height, x: 1800 }}
        bordered
        rowKey="id"
        loading={loading.effects['user/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )
  }
}
