import React from 'react';
import { Form, Input, Modal, Button, Radio, message, Checkbox, Icon, Row, Col} from 'antd';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

import WithModal from '@/hocs/WithModal'

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

@Form.create()
@WithModal({pathname: 'user/fetch'})
export default class FormModal extends React.PureComponent {

  constructor(props) {
    super(props);
  }

  onSubmit = () => {
    this.props.form.validateFields((err, params) => {
      if (!err) {
        this.props.okHandler(params);
      }
    })
  }

  render() {
    const { children, title, loading, record, visible, showModelHandler, hideModelHandler, okHandler } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <span>
        <span onClick={showModelHandler}>
          { children }
        </span>
        <Modal
          title={title}
          visible={visible}
          width={900}
          onCancel={hideModelHandler}
          footer={[
          <Button key="back" type="ghost" size="large" onClick={hideModelHandler}>取消</Button>,
          <Button key="submit" type="primary" size="large" disabled={loading} loading={loading} onClick={this.onSubmit}>
            {loading ? '处理中...' : '确定'}
          </Button>,
        ]}
        >
          <Form layout='horizontal' onSubmit={this.onSubmit}>
            <Row gutter={16}>
              <Col sm={12}>
                <FormItem
                  {...formItemLayout}
                  label="用户名"
                >
                {getFieldDecorator('username', {
                  rules: [
                      { required: true, min: 5, message: '用户名不能小于5位' },
                      { max: 20, message: '用户名不能超过20位' },
                      // { validator: userExists },
                    ],
                    initialValue: record['username'] || ''
                  })(
                  <Input type="text" placeholder="请输入用户名" />
                )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="性别"
                >
                  {getFieldDecorator('gender', { initialValue: record['gender'] || '1' })(
                    <RadioGroup>
                      <Radio value="1">男</Radio>
                      <Radio value="2">女</Radio>
                    </RadioGroup>
                  )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="身份证号"
                >
                  {getFieldDecorator('idcard', {
                    rules: [
                      { max: 20, message: '身份证不能超过20位' },
                      // { validator: userExists },
                    ],
                    initialValue: record['idcard'] || ''
                  })(
                    <Input type="text" placeholder="请输入身份证号码" />
                  )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="联系电话"
                >
                  {getFieldDecorator('phone', {
                    rules: [
                      { min: 5, message: '联系电话不能小于7位' },
                      { max: 20, message: '联系电话不能超过20位' },
                      // { validator: userExists },
                    ],
                    initialValue: record['phone'] || ''
                  })(
                    <Input type="text" placeholder="请输入联系电话" />
                  )}
                </FormItem>
              </Col>

              <Col sm={12}>
                <FormItem
                  {...formItemLayout}
                  label="密码"
                >
                  {getFieldDecorator('password', {
                    rules: [
                      { min: 5, message: '密码不能小于5位' },
                      { max: 32, message: '密码不能超过20位' },
                      // { validator: userExists },
                    ],
                    initialValue: record['password'] || ''
                  })(
                    <Input type="password" placeholder="请输入密码" />
                  )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="邮箱"
                >
                  {getFieldDecorator('email', {
                    rules: [
                      { min: 5, message: '邮箱不能小于5位' },
                      { max: 20, message: '邮箱不能超过20位' },
                      // { validator: userExists },
                    ],
                    initialValue: record['email'] || ''
                  })(
                    <Input type="email" placeholder="请输入邮箱" />
                  )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="工作职位"
                >
                  {getFieldDecorator('job', {
                    rules: [
                      { min: 2, message: '工作职位不能小于5位' },
                      { max: 20, message: '工作职位不能超过20位' },
                      // { validator: userExists },
                    ],
                    initialValue: record['job'] || ''
                  })(
                    <Input type="text" placeholder="请输入工作职位" />
                  )}
                </FormItem>
                <FormItem
                  {...formItemLayout}
                  label="姓名"
                >
                  {getFieldDecorator('name', {
                    rules: [
                      { min: 1, message: '姓名不能小于1个字符' },
                      { max: 5, message: '姓名不能超过5个字符' },
                      // { validator: userExists },
                    ],
                    initialValue: record['name'] || ''
                  })(
                    <Input type="text" placeholder="请输入姓名" />
                  )}
                </FormItem>
              </Col>
            </Row>
          </Form>
        </Modal>
      </span>
    );
  }
}
