import request from '@/utils/axios';
export async function fetch(params) {
  return request({
    url:'/api/employeeSalary/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/employeeSalary',
    method:'post',
    data:params
  });
}

export async function update(params) {
  return request({
    url:`/api/employeeSalary`,
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:'/api/employeeSalary',
    method:'delete',
    data: params
  });
}

export async function queryEmployee(params) {
  return request({
    url:'/api/employeeSalary/read/employee',
    method:'get',
    params
  });
}
