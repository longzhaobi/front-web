import { Tag } from 'antd';
const columns = (toolBar) => {
    return [{
        title: '#',
        width: 50,
        render: (text, record, index) => (
            <span>{index + 1}</span>
        )
    }, {
        title: '姓名',
        dataIndex: 'name',
        width: 160,
    }, {
        title: '职位',
        dataIndex: 'position',
        width: 180,
    }, {
        title: '工资',
        dataIndex: 'money',
        width: 180,
    }, {
        title: '备注',
        dataIndex: 'remark',
    }, {
        title: '维护日期',
        dataIndex: 'updateTime',
        width: 180
    }, {
        title: '操作',
        key: 'operation',
        width: 180,
        render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
