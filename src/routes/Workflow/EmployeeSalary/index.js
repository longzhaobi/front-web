import React, {PropTypes, PureComponent} from 'react';
import {connect} from 'dva';
import {Table, Select, Input, Row, Col, Popconfirm, Divider} from 'antd';
const Search = Input.Search;
import FormModal from './FormModal';

import columns from './columns';

import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

@connect(({employeeSalary, loading}) => ({
  ...employeeSalary,
  namespace: 'employeeSalary',
  loading,
}))
@WithList({pathname: 'employeeSalary/fetch'})
export default class EmployeeSalary extends PureComponent {

  render () {
    const { data, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection } = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    function title() {
      return (
        <Row>
          <Col span={16}>
            <FormModal loading={loading} record={{}} dispatch={dispatch} namespace={namespace} option='create' title="新增薪资信息">
            <IButton type="primary" icon="plus" perm="employeeSalary:create">新增</IButton>
            </FormModal>
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
              <IButton type="danger" disabled={!hasSelected} icon="delete" perm="employeeSalary:remove">删除</IButton>
            </Popconfirm>
            <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <span>
          <FormModal loading={loading} record={record} dispatch={dispatch} namespace={namespace} option='update' title="编辑薪资信息">
            <IButton perm="employeeSalary:update" a> 编辑 </IButton>
          </FormModal>
          <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({id:record.id})}>
            <IButton perm="employeeSalary:remove" a> <Divider type="vertical"/> 删除 </IButton>
          </Popconfirm>
      </span>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        scroll={{ y: table_height }}
        bordered
        size="middle"
        rowKey="id"
        loading={loading.effects['employeeSalary/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )

  }

}
