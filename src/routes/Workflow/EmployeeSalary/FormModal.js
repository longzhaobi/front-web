import React, { Component } from 'react';
import { Form, Input, Modal, Button, InputNumber, Select, DatePicker, Icon, Row, Col, TreeSelect } from 'antd';
import moment from 'moment';
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;

import WithModal from '@/hocs/WithModal'

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

@Form.create()
@WithModal({pathname: 'employeeSalary/fetch'})
export default class FormModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      treeData: [],
    }
  }

  //显示弹出框
  showModelHandler = () => {
    const { dispatch, record } = this.props;
    const _this = this;
    dispatch({type: 'employeeSalary/queryEmployee'}).then(data => {
      if(data) {
        const trees = data.data;
        if(trees) {
          let treeData = [];
          for(let i = 0; i < trees.length; i++) {
            const tree = trees[i];
            treeData[i] = {
              title:tree.name,
              value:tree.id,
              key:tree.id,
              disabled: true
            }
            if(tree.children) {
              let children = [];
              for(let k = 0; k < tree.children.length; k++) {
                const user = tree.children[k];
                children[k] = {
                  title:user.name,
                  value:user.id,
                  key:user.id
                }
              }
              treeData[i].children = children;
            }
          }
          this.setState({ treeData });
        }
        _this.props.showModelHandler();
      }
    })
  }

  //提交表单
  onSubmit = () => {
    this.props.form.validateFields((err, params) => {
      if (!err) {
        this.props.okHandler(params);
      }
    })
  }

  render() {
    const { children, title, loading, record, visible, hideModelHandler, showModelHandler } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <span>
        <span onClick={this.showModelHandler}>
          { children }
        </span>
        <Modal
          title={title}
          visible={visible}
          width={560}
          maskClosable={false}
          onCancel={hideModelHandler}
          footer={[
          <Button key="back" type="ghost" size="large" onClick={hideModelHandler}>取消</Button>,
          <Button key="submit" type="primary" size="large" disabled={loading} loading={loading} onClick={this.onSubmit}>
            {loading ? '处理中...' : '确定'}
          </Button>,
        ]}
        >
          <Form layout='horizontal' onSubmit={this.onSubmit}>
            <FormItem
              {...formItemLayout}
              label="员工薪水"
            >
              {getFieldDecorator('userId', {
                rules:[
                  { required: true, message: '请输入员工薪水' },
                ],
                initialValue: record['userId']
              })(
                <TreeSelect
                  showSearch
                  dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                  treeData={this.state.treeData}
                  allowClear
                  placeholder="请选择人员信息"
                  // onChange={this.onChange}
                />
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="员工薪水"
            >
              {getFieldDecorator('money', {
                rules:[
                  { required: true, message: '请输入员工薪水' },
                ],
                initialValue: record['money'] || 0
              })(
                <Input placeholder="请输入员工薪水"/>
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="备注"
            >
              {getFieldDecorator('remark', {
                rules: [
                   { max: 250, message: '不能超过250位字符' }
                 ],
                 initialValue: record['remark'] || ''
               })(
                <TextArea rows={4} placeholder="请输入备注信息" />
              )}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}
