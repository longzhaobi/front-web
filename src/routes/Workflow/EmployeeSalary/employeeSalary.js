import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

export default modelExtend(grid(service, '/workflow/employeeSalary', 'employeeSalary'), {
  namespace: 'employeeSalary',

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const o = yield select(({ employeeSalary }) => employeeSalary);
      const params = {
        current: o.current,
        size: o.size,
        keyword: o.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    },
    *queryEmployee({ payload }, { call, put, select }) {
      return yield call(service.queryEmployee, payload);
    },
    
  }
});
