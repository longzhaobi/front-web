import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

export default modelExtend(grid(service, '/workflow/task', 'task'), {
  namespace: 'task',

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const o = yield select(({ task }) => task);
      const params = {
        current: o.current,
        size: o.size,
        keyword: o.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    },
    *fetchTaskDetail({ payload }, { call}) {
      return yield call(service.fetchTaskDetail, payload)
    },
    *approve({ payload }, { call}) {
      return yield call(service.commitApprove, payload)
    },
  }
});
