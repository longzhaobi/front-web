import React, { Component } from 'react';
import { Form, Input, Modal, Button, Select, Radio, Row, Col} from 'antd';
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

import WithModal from '@/hocs/WithModal'
import styles from './index.css';
const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

@Form.create()
@WithModal({pathname: 'task/fetch'})
export default class FormModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      leaveType: [],
      comments:[],
      formFields:[],
      approver:[],
      hide: true, //默认不需要下一步审批
    }
  }
  //提交表单
  onSubmit = () => {
    const { entityId, processInstanceId, taskId, processType } = this.props.record;
    this.props.form.validateFields((err, params) => {
      if (!err) {
        const p = {...params, entityId, processInstanceId, taskId, processType };
        this.props.okHandler(p, '确定后，将自动发起请假流程，请问继续吗？');
      }
    })
  }

  //显示弹出框
  showModelHandler = () => {
    const { dispatch, record } = this.props;
    const _this = this;
    dispatch({type: 'task/fetchTaskDetail', payload: {taskId: record.taskId, processType: record.processType, activityId: record.activityId, entityId: record.entityId}}).then(data => {
      if(data) {
        const { data:{comments, formFields, approver} } = data;
        _this.setState({comments, formFields, approver, hide:approver.length === 0});
        this.props.showModelHandler();
      }
    })
  }

  createFormFields = (datas) => datas.map(item => {
    return <Col span={12} key={item.filedText}>
            <FormItem
              {...formItemLayout}
              label={item.filedText}
              >
              <Input value={item.fieldValue} readOnly={true}/>
            </FormItem>
          </Col>
  });

  createComments = (datas) => datas.map(item => {
    return  <Col span={12} key={item.id}><FormItem
            {...formItemLayout}
            label='评论信息：'
          >
          <div className={styles['comment-div']} dangerouslySetInnerHTML={{__html:item.content}}></div>
      </FormItem></Col>
  })

  /**
   * 组装下一步审批人下拉选择
   */
  packageNextSelect= (datas) => datas.map(item => {
    return <Option key = {item.id}>{item.name}</Option>
  })

  onRadioChange = (e) => {
    if(e.target.value == 0) {
      this.setState({hide:true});
    }
  }
  render() {
    const { children, title, loading, record, visible, hideModelHandler } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <div style={{display:'inline-block'}}>
        <span onClick={this.showModelHandler}>
          { children }
        </span>
        <Modal
          title={title}
          visible={visible}
          className = {styles['form-modal']}
          width={800}
          maskClosable={false}
          onCancel={hideModelHandler}
          footer={[
          <Button key="back" type="ghost" size="large" onClick={hideModelHandler}>取消</Button>,
          <Button key="submit" type="primary" size="large" disabled={loading} loading={loading} onClick={this.onSubmit}>
            {loading ? '处理中...' : '确定'}
          </Button>,
        ]}
        >
          <Form layout='horizontal' onSubmit={this.onSubmit}>
            <div className={styles['form-in-div']}>
              <Row gutter={24}>{this.createFormFields(this.state.formFields)}</Row>
              <Row gutter={24}>{this.createComments(this.state.comments)}</Row>
            </div>
            <FormItem
              {...formItemLayout}
              label="是否通过"
            >
              {getFieldDecorator('pass', {
                rules: [
                  {required:true, message: '请选择是否通过'}
                ]
              })(
                <RadioGroup onChange={this.onRadioChange}>
                  <Radio value="1">通过</Radio>
                  <Radio value="0">打回</Radio>
                </RadioGroup>
              )}
            </FormItem>
            {
              this.state.hide ? null : <FormItem
                {...formItemLayout}
                label="审批人"
              >
              {getFieldDecorator('nextUserId', {
                rules: [
                    { required: true, message: '请选择审批人' }
                  ],
                  initialValue: record['nextUserId'] || null
                })(
                  <Select
                    placeholder="请选择审批人"
                  >
                    {this.packageNextSelect(this.state.approver)}
                  </Select>
              )}
              </FormItem>
            }
            <FormItem
              {...formItemLayout}
              label="审批意见"
            >
              {getFieldDecorator('content', {
                rules: [
                   { required: true, message: '请输入审批意见' },
                   { max: 250, message: '不能超过250位字符' }
                 ],
                 initialValue: record['content'] || ''
               })(
                <TextArea rows={4} placeholder="请输入请假事由" />
              )}
            </FormItem>
          </Form>
        </Modal>
      </div>
    );
  }
}
