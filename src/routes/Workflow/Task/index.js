import React, {PropTypes, PureComponent} from 'react';
import {connect} from 'dva';
import {Table, Select, Input, Button, Row, Col, Popconfirm, Icon, Divider} from 'antd';
const Search = Input.Search;

import WorkflowPanel from '@/components/WorkflowPanel';

import FormModal from './FormModal';

import columns from './columns';

import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

@connect(({task, loading}) => ({
  ...task,
  namespace: 'task',
  loading,
}))
@WithList({pathname: 'task/fetch'})
export default class Task extends PureComponent {

  render () {
    const { data, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection } = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    function title() {
      return (
        <Row>
          <Col span={16}>

          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <span>
          <FormModal loading={loading} record={record} dispatch={dispatch} namespace={namespace} option='approve' title="申请审批">
            <IButton perm="task:approve" a> 审批 </IButton>
          </FormModal>
          <WorkflowPanel id = {record.entityId} dispatch={dispatch} >
            <IButton perm="workflow:view" a> <Divider type="vertical"/> 查看 </IButton>
          </WorkflowPanel>
      </span>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        scroll={{ y: table_height }}
        bordered
        size="middle"
        rowKey="taskId"
        loading={loading.effects['task/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )

  }

}
