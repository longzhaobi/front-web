import { Tag } from 'antd';
const columns = (toolBar) => {
    return [{
        title: '#',
        width: 50,
        render: (text, record, index) => (
            <span>{index + 1}</span>
        )
    }, {
        title: '任务标题',
        dataIndex: 'title',
        width: 240,
    }, {
        title: '申请日期',
        dataIndex: 'approveDate',
        width: 180,
    }, {
        title: '申请人',
        dataIndex: 'approveName',
        width: 180,
    }, {
      title: '当前状态',
      dataIndex: 'dataStatus',
      width: 180,
    }, {
        title: '申请类型',
        dataIndex: 'processType',
        render: (text) => {
            if(text === 'LeaveProcess') {
              return '请假申请';
            } else if(text === 'ExpenseAccountProcess') {
              return '报销申请';
            } else if(text === 'SalaryAdjustProcess') {
              return '薪资调整申请';
            } else {
              return '其他'
            }
        }
        // width:140,
    }, {
        title: '操作',
        key: 'operation',
        width: 150,
        render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
