import request from '@/utils/axios';
export async function fetch(params) {
  return request({
    url:'/api/workflow/read/task/page',
    params
  });
}

export async function fetchTaskDetail(params) {
  return request({
    url:'/api/workflow/read/task/detail',
    params
  });
}

//提交审批
export async function commitApprove(params) {
  return request({
    url:'/api/workflow/task/approve',
    method: 'post',
    data:params
  });
}