import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

export default modelExtend(grid(service, '/workflow/salary/salaryAdjust', 'salaryAdjust'), {
  namespace: 'salaryAdjust',

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const o = yield select(({ salaryAdjust }) => salaryAdjust);
      const params = {
        current: o.current,
        size: o.size,
        keyword: o.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    },

  }
});
