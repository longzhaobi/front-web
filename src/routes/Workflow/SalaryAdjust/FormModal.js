import React, { PureComponent } from 'react';
import { Form, Input, Modal, Button, InputNumber, Select, DatePicker, Col} from 'antd';
import moment from 'moment';
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;

import WithModal from '@/hocs/WithModal'

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

@Form.create()
@WithModal({pathname: 'salaryAdjust/fetch'})
export default class FormModal extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      approver:[],
    }
  }

  //提交表单
  onSubmit = () => {
    this.props.form.validateFields((err, params) => {
      if (!err) {
        this.props.okHandler(params, '确定后，将自动发起薪资调整流程，请问继续吗？');
      }
    })
  }

  //显示弹出框
  showModelHandler = () => {
    this.fetchApprover();
  }

  /**
   * 组装下一步审批人下拉选择
   */
  packageNextSelect= (datas) => datas.map(item => {
    return <Option key = {item.id}>{item.name}</Option>
  })

  //获取审批人
  fetchApprover = () => {
    const _this = this;
    //获取审批人前清空选项
    this.props.form.resetFields(['nextUserId'])
    this.props.dispatch({type: 'app/fetchApprover', payload: {role:'director'}}).then(data => {
      if(data) {
        _this.setState({
          approver:data.data
        });
        _this.props.showModelHandler();
      }
    })
  }

  render() {
    const { children, title, loading, record, visible, hideModelHandler } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <span>
        <span onClick={this.showModelHandler}>
          { children }
        </span>
        <Modal
          title={title}
          visible={visible}
          width={560}
          maskClosable={false}
          onCancel={hideModelHandler}
          footer={[
          <Button key="back" type="ghost" size="large" onClick={hideModelHandler}>取消</Button>,
          <Button key="submit" type="primary" size="large" disabled={loading} loading={loading} onClick={this.onSubmit}>
            {loading ? '处理中...' : '确定'}
          </Button>,
        ]}
        >
          <Form layout='horizontal' onSubmit={this.onSubmit}>
            <FormItem
              {...formItemLayout}
              label="调薪金额"
            >
            {getFieldDecorator('money', {
              rules: [
                  { required: true, message: '请输入调薪金额' }
                ],
                initialValue: record['money'] || 0
              })(
                <Input placeholder="请输入调薪金额" />
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="审批人"
            >
            {getFieldDecorator('nextUserId', {
              rules: [
                  { required: true, message: '请选择审批人' }
                ],
                initialValue: record['nextUserId'] || null
              })(
                <Select
                  placeholder="请选择审批人"
                  notFoundContent="暂无审批人信息"
                >
                  {this.packageNextSelect(this.state.approver)}
                </Select>
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="备注"
            >
              {getFieldDecorator('remark', {
                rules: [
                   { required: true, message: '请输入调薪备注信息' },
                   { max: 250, message: '不能超过250位字符' }
                 ],
                 initialValue: record['remark'] || ''
               })(
                <TextArea rows={4} placeholder="请输入调薪备注信息" />
              )}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}
