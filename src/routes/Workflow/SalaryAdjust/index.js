import React, {PropTypes, PureComponent} from 'react';
import {connect} from 'dva';
import {Table, Select, Input, Row, Col, Popconfirm, Divider} from 'antd';
const Search = Input.Search;
import WorkflowPanel from '@/components/WorkflowPanel';
import FormModal from './FormModal';

import columns from './columns';

import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

@connect(({salaryAdjust, loading}) => ({
  ...salaryAdjust,
  namespace: 'salaryAdjust',
  loading,
}))
@WithList({pathname: 'salaryAdjust/fetch'})
export default class SalaryAdjust extends PureComponent {

  render () {
    const { data, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection } = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    function title() {
      return (
        <Row>
          <Col span={16}>
            <FormModal loading={loading} record={{}} dispatch={dispatch} namespace={namespace} option='create' title="新增请假">
            <IButton type="primary" icon="plus" perm="salaryAdjust:create">新增</IButton>
            </FormModal>
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
              <IButton type="danger" disabled={!hasSelected} icon="delete" perm="salaryAdjust:remove">删除</IButton>
            </Popconfirm>
            <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <span>
          <FormModal loading={loading} record={record} dispatch={dispatch} namespace={namespace} option='update' title="编辑报销申请">
            <IButton perm="salaryAdjust:update" disabled={record.dataStatus !== '2'} a> 编辑 </IButton>
          </FormModal>
          <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({id:record.id})}>
            <IButton perm="salaryAdjust:remove" a> <Divider type="vertical"/> 删除 </IButton>
          </Popconfirm>
          <WorkflowPanel id = {record.id} dispatch={dispatch} title='报销审批流程轴' >
            <IButton perm="workflow:view" a> <Divider type="vertical"/> 查看 </IButton>
          </WorkflowPanel>
      </span>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        expandedRowRender={record => <p style={{ margin: 0 }}>{record.reason}</p>}
        rowSelection={rowSelection}
        scroll={{ y: table_height }}
        bordered
        size="middle"
        rowKey="id"
        loading={loading.effects['salaryAdjust/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )

  }

}
