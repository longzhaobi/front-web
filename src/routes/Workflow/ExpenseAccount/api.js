import request from '@/utils/axios';
export async function fetch(params) {
  return request({
    url:'/api/expenseAccount/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/expenseAccount',
    method:'post',
    data:params
  });
}

export async function update(params) {
  return request({
    url:`/api/expenseAccount`,
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:'/api/expenseAccount',
    method:'delete',
    data: params
  });
}

//获取资源和权限表头
export async function fetchResources() {
  return request({
    url:'/api/resources'
  });
}

export async function fetchColumns() {
  return request({
    url:'/api/expenseAccount/columns'
  });
}
