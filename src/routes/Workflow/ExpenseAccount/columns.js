import { Tag } from 'antd';
const columns = (toolBar) => {
    return [{
        title: '#',
        width: 50,
        render: (text, record, index) => (
            <span>{index + 1}</span>
        )
    },{
        title: '费用项目',
        dataIndex: 'expenseItem',
        width: 180,
    }, {
        title: '工作日期',
        dataIndex: 'workDate',
        width: 180,
    }, {
      title: '报销金额',
      dataIndex: 'money',
      width: 180,
    }, {
        title: '当前流程',
        dataIndex: 'dataStatus',
        width: 140,
        render: (text) => {
          switch(text) {
            case '0':
                return <Tag color="#2db7f5">提交审批中</Tag>
                break;
            case '1':
                return <Tag color="#2db7f5">财务审批通过</Tag>
                break;
            case '2':
                return <Tag color="#2db7f5">财务审批打回</Tag>
                break;
            case '3':
                return <Tag color="#2db7f5">银行转账出错，转现金支付</Tag>
                break;
            default:
                return <Tag color="#2db7f5">其他</Tag>
          }
        }
    }, {
      title: '报销状态',
      width: 140,
      render: (text, record) => {
        switch(record.dataStatus) {
          case '1':
              return <Tag color="#2db7f5">已报销</Tag>
          default:
              return <Tag color="#2db7f5">未报销</Tag>
        }
      }
    }, {
        title: '维护日期',
        dataIndex: 'updateTime',
        width: 180
    }, {
        title: '操作',
        key: 'operation',
        width: 180,
        render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
