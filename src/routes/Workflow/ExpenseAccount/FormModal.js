import React, { PureComponent } from 'react';
import { Form, Input, Modal, Button, InputNumber, Select, DatePicker, Col} from 'antd';
import moment from 'moment';
const { TextArea } = Input;
const FormItem = Form.Item;
const Option = Select.Option;

import WithModal from '@/hocs/WithModal'

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};

@Form.create()
@WithModal({pathname: 'expenseAccount/fetch'})
export default class FormModal extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      approver:[],
    }
  }

  //提交表单
  onSubmit = () => {
    this.props.form.validateFields((err, params) => {
      if (!err) {
        const p = {
          ...params,
          workDate:params['workDate'].format('YYYY-MM-DD'),
        }
        this.props.okHandler(p, '确定后，将自动发起报销流程，请问继续吗？');
      }
    })
  }

  //显示弹出框
  showModelHandler = () => {
    this.fetchApprover();
  }

  /**
   * 组装下一步审批人下拉选择
   */
  packageNextSelect= (datas) => datas.map(item => {
    return <Option key = {item.id}>{item.name}</Option>
  })

  //获取审批人
  fetchApprover = () => {
    const _this = this;
    //获取审批人前清空选项
    this.props.form.resetFields(['nextUserId'])
    this.props.dispatch({type: 'app/fetchApprover', payload: {role:'finance'}}).then(data => {
      if(data) {
        _this.setState({
          approver:data.data
        });
        _this.props.showModelHandler();
      }
    })
  }

  //限制日期选择，不能选择大于今天的日期
  disabledDate = (startValue) => {
    return startValue && startValue > moment().endOf('day');
  }

  render() {
    const { children, title, loading, record, visible, hideModelHandler } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <span>
        <span onClick={this.showModelHandler}>
          { children }
        </span>
        <Modal
          title={title}
          visible={visible}
          width={560}
          maskClosable={false}
          onCancel={hideModelHandler}
          footer={[
          <Button key="back" type="ghost" size="large" onClick={hideModelHandler}>取消</Button>,
          <Button key="submit" type="primary" size="large" disabled={loading} loading={loading} onClick={this.onSubmit}>
            {loading ? '处理中...' : '确定'}
          </Button>,
        ]}
        >
          <Form layout='horizontal' onSubmit={this.onSubmit}>
            <FormItem
              {...formItemLayout}
              label="费用项目"
            >
            {getFieldDecorator('expenseItem', {
              rules: [
                  { required: true, message: '请输入费用项目' }
                ],
                initialValue: record['expenseItem'] || null
              })(
                <Input placeholder="请输入费用项目" />
            )}
            </FormItem>
            <FormItem
              label="工作日期"
              {...formItemLayout}
            >
              {getFieldDecorator('workDate', {
                rules: [
                  {required: true, message: '请选择工作日期'}
                ],
                initialValue: record['workDate'] && moment(record['workDate'], 'YYYY-MM-DD')
              })(
                <DatePicker disabledDate={this.disabledDate}/>
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="报销金额"
              help="单位元"
            >
              {getFieldDecorator('money', {
                initialValue: record['money'] || 0
              })(
                <Input placeholder="请输入报销金额" />
              )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="请假审批人"
            >
            {getFieldDecorator('nextUserId', {
              rules: [
                  { required: true, message: '请选择审批人' }
                ],
                initialValue: record['nextUserId'] || null
              })(
                <Select
                  placeholder="请选择审批人"
                  notFoundContent="暂无审批人信息"
                >
                  {this.packageNextSelect(this.state.approver)}
                </Select>
            )}
            </FormItem>
            <FormItem
              {...formItemLayout}
              label="报销事由"
            >
              {getFieldDecorator('reason', {
                rules: [
                   { required: true, message: '请输入报销事由' },
                   { max: 250, message: '不能超过250位字符' }
                 ],
                 initialValue: record['reason'] || ''
               })(
                <TextArea rows={4} placeholder="请输入报销事由" />
              )}
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}
