import { Tag } from 'antd';
const columns = (toolBar) => {
    return [{
        title: '#',
        width: 50,
        render: (text, record, index) => (
            <span>{index + 1}</span>
        )
    }, {
        title: '请假类型',
        dataIndex: 'leaveType',
        width: 100,
        render: (text) => {
            switch(text) {
                case '1':
                    return <Tag color="#2db7f5">事假</Tag>
                    break;
                case '2':
                    return <Tag color="#2db7f5">病假</Tag>
                    break;
                case '3':
                    return <Tag color="#2db7f5">婚假</Tag>
                    break;
                case '4':
                    return <Tag color="#2db7f5">丧假</Tag>
                    break;
                case '5':
                    return <Tag color="#2db7f5">公假</Tag>
                    break;
                case '6':
                    return <Tag color="#2db7f5">工伤</Tag>
                    break;
                case '7':
                    return <Tag color="#2db7f5">产假</Tag>
                    break;
                case '99':
                    return <Tag color="#2db7f5">其他</Tag>
                    break;
                default:
                    return <Tag color="#2db7f5">其他</Tag>
            }

        }
    }, {
        title: '开始时间',
        dataIndex: 'leaveBeginDate',
        width: 180,
    }, {
        title: '结束时间',
        dataIndex: 'leaveEndDate',
        width: 180,
    }, {
        title: '当前流程',
        dataIndex: 'dataStatus',
        render: (text) => {
          switch(text) {
            case '0':
                return <Tag color="#2db7f5">提交审批中</Tag>
                break;
            case '1':
                return <Tag color="#2db7f5">经理/总监审批通过</Tag>
                break;
            case '2':
                return <Tag color="#2db7f5">人事审批通过</Tag>
                break;
            case '3':
                return <Tag color="#2db7f5">经理/总监打回</Tag>
                break;
            case '4':
                return <Tag color="#2db7f5">人事打回</Tag>
                break;
            default:
                return <Tag color="#2db7f5">其他</Tag>
          }
        }
    }, {
      title: '请假状态',
      width: 140,
      render: (text, record) => {
        switch(record.dataStatus) {
          case '2':
              return <Tag color="#2db7f5">已准假</Tag>
          default:
              return <Tag color="#2db7f5">未准假</Tag>
        }
      }
    }, {
        title: '维护日期',
        dataIndex: 'updateTime',
        width: 180
    }, {
        title: '操作',
        key: 'operation',
        width: 180,
        render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
