import React, {PropTypes, PureComponent} from 'react';
import {connect} from 'dva';
import {Table, Select, Input, Row, Col, Popconfirm, Divider} from 'antd';
const Search = Input.Search;
import WorkflowPanel from '@/components/WorkflowPanel';
import FormModal from './FormModal';

import columns from './columns';

import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

@connect(({leave, loading}) => ({
  ...leave,
  namespace: 'leave',
  loading,
}))
@WithList({pathname: 'leave/fetch'})
export default class Leave extends PureComponent {

  render () {
    const { data, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection } = this.props;
    const hasSelected = selectedRowKeys.length > 0;
    function title() {
      return (
        <Row>
          <Col span={16}>
            <FormModal loading={loading} record={{}} dispatch={dispatch} namespace={namespace} option='create' title="新增请假">
            <IButton type="primary" icon="plus" perm="leave:create">新增</IButton>
            </FormModal>
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
              <IButton type="danger" disabled={!hasSelected} icon="delete" perm="leave:remove">删除</IButton>
            </Popconfirm>
            <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <span>
          <FormModal loading={loading} record={record} dispatch={dispatch} namespace={namespace} option='update' title="编辑权限">
            <IButton perm="leave:update" disabled={record.dataStatus !== '3'} a> 编辑 </IButton>
          </FormModal>
          <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({id:record.id})}>
            <IButton perm="leave:remove" a> <Divider type="vertical"/> 删除 </IButton>
          </Popconfirm>
          <WorkflowPanel id = {record.id} dispatch={dispatch} title='请假审批流程轴' >
            <IButton perm="workflow:view" a> <Divider type="vertical"/> 查看 </IButton>
          </WorkflowPanel>
      </span>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        expandedRowRender={record => <p style={{ margin: 0 }}>{record.leaveReason}</p>}
        pagination={false}
        rowSelection={rowSelection}
        scroll={{ y: table_height }}
        bordered
        size="middle"
        rowKey="id"
        loading={loading.effects['leave/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )

  }

}
