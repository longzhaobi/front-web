import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux, Link } from 'dva/router';

import { Form, Input, Tabs, Button, Icon, Checkbox, Row, Col, Alert } from 'antd';
import styles from './Login.less';
import Cookies from 'js-cookie';
const FormItem = Form.Item;
const { TabPane } = Tabs;

import WithExplore from '@/hocs/WithExplore';

@connect(({app, loading}) => ({
  app,
  namespace: 'app',
  loading
}))
@WithExplore
@Form.create()
export default class Login extends Component {
  state = {
    count: 0,
    type: 'account',
    loginText: '登录'
  }

  componentDidMount() {
    //每次进入登录页面时，初始化信息
    localStorage.clear();
    this.props.dispatch({type:'app/clearLoginInfo'});
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  //切换登录方式
  onSwitch = (key) => {
    this.setState({
      type: key,
    });
  }

  //获取验证码倒计时
  onGetCaptcha = () => {
    let count = 59;
    this.setState({ count });
    this.interval = setInterval(() => {
      count -= 1;
      this.setState({ count });
      if (count === 0) {
        clearInterval(this.interval);
      }
    }, 1000);
  }

  //表单提交
  handleSubmit = (e) => {
    e.preventDefault();
    const { type } = this.state;
    const { namespace } = this.props;
    this.props.form.validateFields({ force: true },
      (err, values) => {
        if (!err) {
          this.props.dispatch({
            type: `${namespace}/login`,
            payload: values,
          }).then((response) => {
            if(response) {
              this.setState({loginText: '正在跳转...'})
              const { token, sysUser, hasPermissions, signToken } = response.data;
              Cookies.set("loozb_token", token);
              localStorage.avatorPath = sysUser.avatar;
              localStorage.username = sysUser.username;
              localStorage.userId = sysUser.id;
              localStorage.signToken = signToken;
              //保存签名token,参数请求时不传输，用来加密参数
              // sessionStorage.setItem("signToken", signToken);
              localStorage.permission = JSON.stringify(hasPermissions);
              this.props.dispatch(routerRedux.push('/home'));
            }
          });
        }
      }
    );
  }

  renderMessage = (message) => {
    return (
      <Alert
        style={{ marginBottom: 24 }}
        message={message}
        type="error"
        showIcon
      />
    );
  }

  render() {
    const { form, app, namespace, loading } = this.props;
    const { getFieldDecorator } = form;
    const { count, type } = this.state;
    const fetching = loading.effects['app/login'];
    return (
      <div className={styles.main}>
        <Form onSubmit={this.handleSubmit}>
          <Tabs animated={false} className={styles.tabs} activeKey={type} onChange={this.onSwitch}>
            <TabPane tab="账户密码登录" key="account">
              {
                app.status === 'error' &&
                app.type === 'account' &&
                app.submitting === false &&
                this.renderMessage('账户或密码错误')
              }
              <FormItem>
                {getFieldDecorator('account', {
                  rules: [{
                    required: type === 'account', message: '请输入账户名！',
                  }],
                  initialValue:'admin'
                })(
                  <Input
                    size="large"
                    prefix={<Icon type="user" className={styles.prefixIcon} />}
                    placeholder="请输入账户名"
                  />
                )}
              </FormItem>
              <FormItem>
                {getFieldDecorator('password', {
                  rules: [{
                    required: type === 'account', message: '请输入密码！',
                  }],
                  initialValue:'123456'
                })(
                  <Input
                    size="large"
                    prefix={<Icon type="lock" className={styles.prefixIcon} />}
                    type="password"
                    placeholder="请输入密码"
                  />
                )}
              </FormItem>
            </TabPane>
            <TabPane tab="手机号登录" key="mobile">
              {
                app.status === 'error' &&
                app.type === 'mobile' &&
                app.submitting === false &&
                this.renderMessage('验证码错误')
              }
              <FormItem>
                {getFieldDecorator('mobile', {
                  rules: [{
                    required: type === 'mobile', message: '请输入手机号！',
                  }, {
                    pattern: /^1\d{10}$/, message: '手机号格式错误！',
                  }],
                })(
                  <Input
                    size="large"
                    prefix={<Icon type="mobile" className={styles.prefixIcon} />}
                    placeholder="手机号"
                  />
                )}
              </FormItem>
              <FormItem>
                <Row gutter={8}>
                  <Col span={16}>
                    {getFieldDecorator('captcha', {
                      rules: [{
                        required: type === 'mobile', message: '请输入验证码！',
                      }],
                    })(
                      <Input
                        size="large"
                        prefix={<Icon type="mail" className={styles.prefixIcon} />}
                        placeholder="验证码"
                      />
                    )}
                  </Col>
                  <Col span={8}>
                    <Button
                      disabled={count >0}
                      className={styles.getCaptcha}
                      size="large"
                      onClick={this.onGetCaptcha}
                    >
                      {count ? `${count} s` : '获取验证码'}
                    </Button>
                  </Col>
                </Row>
              </FormItem>
            </TabPane>
          </Tabs>
          <FormItem className={styles.additional}>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(
              <Checkbox className={styles.autoLogin}>自动登录</Checkbox>
            )}
            <a className={styles.forgot} href="">忘记密码</a>
            <Button size="large" loading={fetching} className={styles.submit} type="primary" htmlType="submit">
              {fetching ? '跳转中...' : '登录'}
            </Button>
          </FormItem>
        </Form>
        <div className={styles.other}>
          其他登录方式
          {/* 需要加到 Icon 中 */}
          <span className={styles.iconAlipay} />
          <span className={styles.iconTaobao} />
          <span className={styles.iconWeibo} />
          <Link className={styles.register} to="/user/register">注册账户</Link>
        </div>
      </div>
    );
  }
}
