import React, { PureComponent } from 'react';
import {connect} from 'dva';
import {Table, Input, Row, Col, Popconfirm} from 'antd';
const Search = Input.Search;

import WithList from '@/hocs/WithList';

@connect(({loginLog, loading}) => ({
  ...loginLog,
  namespace: '',
  loading,
}))
@WithList({pathname: 'loginLog/fetch'})
export default class LoginLog extends PureComponent {

  render () {

    const {data, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection} = this.props;
    function title() {
      return (
        <Row>
          <Col span={16}>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <div>
        {isAuth('log:remove') ? (
          <span>
            <span className="ant-divider" />
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({id:record.id})}>
              <a href="javascript:void(0)">删除</a>
            </Popconfirm>
          </span>) : ''}
      </div>
    )

    const columns = [{
      title: '#',
      fixed:'left',
      width:50 ,
      render:(text, record, index) => (
        <span>{index + 1}</span>
      )
    },{
      title: '登录用户',
      dataIndex: 'username',
      width:140,
    }, {
      title: '登录IP',
      dataIndex: 'loginIp',
      width:180,
    },{
      title: '登录时间',
      dataIndex: 'loginTime',
      width:220
    }, {
      title: '浏览器',
      dataIndex: 'browserInfo',
    }];
    return (
      <Table
        columns={columns}
        dataSource={data}
        rowSelection={rowSelection}
        pagination={false}
        size="middle"
        scroll={{ y: table_height }}
        bordered
        rowKey="id"
        loading={loading.effects['loginLog/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )
  }
}
