import request from '@/utils/axios';
export async function fetch(params) {
  return request({
    url:'/api/loginLog/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/loginLog',
    method:'post',
    data:params
  });
}

export async function update(params) {
  return request({
    url:`/api/loginLog`,
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:'/api/loginLog',
    method:'delete',
    data: params
  });
}
