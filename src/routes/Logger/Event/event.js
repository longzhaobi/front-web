import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

export default modelExtend(grid(service, '/logger/event', 'event') ,{
  namespace: 'event',

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const o = yield select(({ event }) => event);
      const params = {
        current: o.current,
        size: o.size,
        keyword: o.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    }

  }
});
