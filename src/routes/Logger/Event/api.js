import request from '@/utils/axios';
import qs from 'qs';
export async function fetch(params) {
  return request({
    url:'/api/events/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/events',
    method:'post',
    data:params
  });
}

export async function update(params) {
  return request({
    url:`/api/events`,
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:'/api/events',
    method:'delete',
    data: params
  });
}
