import React, { PureComponent } from 'react';
import {connect} from 'dva';
import {Table, Input, Row, Col, Popconfirm} from 'antd';
const Search = Input.Search;

import WithList from '@/hocs/WithList';
import {formatJson} from '@/utils/utils';

@connect(({event, loading}) => ({
  ...event,
  namespace: 'event',
  loading,
}))
@WithList({pathname: 'event/fetch'})
export default class Event extends PureComponent {

  render () {

    const {data, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection} = this.props;
    function title() {
      return (
        <Row>
          <Col span={16}>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <div>
        {isAuth('log:remove') ? (
          <span>
            <span className="ant-divider" />
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({id:record.id})}>
              <a href="javascript:void(0)">删除</a>
            </Popconfirm>
          </span>) : ''}
      </div>
    )

    const columns = [{
      title: '日志标题',
      dataIndex: 'title',
      width:320
    }, {
      title: '操作用户',
      dataIndex: 'name',
      width:140,
    }, {
      title: '请求URL',
      dataIndex: 'requestUri',
      width:220,
    },{
      title: '请求方法',
      dataIndex: 'method',
      width:120
    }, {
      title: '耗时(毫秒)',
      dataIndex: 'timeConsuming',
      width:120,
    }, {
      title: '请求IP',
      dataIndex: 'clientHost',
      width:180,
    }, {
      title: '请求客户端',
      dataIndex: 'userAgent',
      width:300,
    },{
      title: '请求状态',
      dataIndex: 'status',
      width:120
    }, {
      title: '创建日期',
      dataIndex: 'createTime',
      width:180
    }, {
      title: '维护日期',
      dataIndex: 'mtime',
      width:180
    }];
    return (
      <Table
        columns={columns}
        expandedRowRender={record => <pre>{<div dangerouslySetInnerHTML={{__html:formatJson(record.parameters)}}></div>}</pre>}
        dataSource={data}
        pagination={false}
        size="middle"
        scroll={{ y: table_height-3, x:1790 }}
        bordered
        rowKey="id"
        loading={loading.effects['event/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )
  }
}
