import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

export default modelExtend(grid(service, '/logger/sysLog', 'sysLog'), {
  namespace: 'sysLog',

  state: {
    loading: false
  },

  reducers: {
    fetchSuccess(state, { payload: { data, keyword } }) {
      return { ...state, data, selectedRowKeys:[], keyword, loading: false };
    },
    chnageLoading(state, {payload}) {
      return { ...state, loading:payload}
    }
  },
  effects: {
    *fetch({ payload = {} }, { call, put, select }) {
      yield put({type: 'chnageLoading', payload: true})
      const data = yield call(service.fetch, payload);
      if(data) {
        yield put({
          type: 'fetchSuccess',
          payload: {data:data.data, keyword: payload.keyword}
        });
      }
    },
    *reload(action, { put, select }) {
      yield put({ type: 'fetch' });
    }

  }
});
