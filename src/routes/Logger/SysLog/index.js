import React, {PropTypes, PureComponent} from 'react';
import {connect} from 'dva';
import {Table, Popconfirm} from 'antd';

import columns from './columns';


import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

@connect(({sysLog, loading}) => ({
  ...sysLog,
  namespace: 'sysLog',
  loading,
}))
@WithList({pathname: 'sysLog/fetch'})
export default class User extends PureComponent {

  render() {
    const {data, current, total, size, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection} = this.props;
    const hasSelected = selectedRowKeys.length > 0;

    function onDownloadFile(filePath, fileName) {
      window.location.href = `${fileUrl}/events/downFile?fileName=${fileName}&filePath=${filePath}`
    }

    const toolBar= (text, record, index) => (
      <div>
        <Popconfirm title="确定要下载吗？" onConfirm={() => onDownloadFile(record.filePath, record.fileName)}>
          <IButton perm="resource:remove" a>下载日志 </IButton>
        </Popconfirm>
      </div>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        size="middle"
        scroll={{ y: table_height + 100 }}
        bordered
        rowKey="id"
        loading={loading.effects['sysLog/fetch']}
      />
    )
  }
}
