import { Tag } from 'antd';
const columns = (toolBar) => {
    return [{
        title: '文件名称',
        dataIndex: 'fileName',
        width: 340,
    },{
        title: '是否是目录',
        dataIndex: 'isDirectory',
        width: 140,
        render: (text) => text === 'Y' ? <Tag color="#2db7f5">是</Tag> : <Tag color="#f50">否</Tag>
    }, {
        title: '文件路径',
        dataIndex: 'filePath',
    }, {
        title: '文件时间',
        dataIndex: 'fileTime',
        width: 180
    }, {
        title: '文件大小(KB)',
        dataIndex: 'fileSize',
        width: 180,
    }, {
        title: '操作',
        key: 'operation',
        width: 180,
        render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
