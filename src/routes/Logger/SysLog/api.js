import request from '@/utils/axios';
export async function fetch(params) {
  return request({
    url:`/api/events/fileList`,
    params
  });
}
