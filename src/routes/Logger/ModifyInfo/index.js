import React, {PropTypes, PureComponent} from 'react';
import { routerRedux } from 'dva/router';
import {connect} from 'dva';
import {Table, Select, Input, Button, Row, Col, Popconfirm, Icon} from 'antd';
const Option = Select.Option;
const Search = Input.Search;

import styles from './index.css';

import columns from './columns';


import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

@connect(({modifyInfo, loading}) => ({
  ...modifyInfo,
  namespace: 'modifyInfo',
  loading,
}))
@WithList({pathname: 'modifyInfo/fetch'})
export default class Modify extends PureComponent {

  render () {

    const {data, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection } = this.props;
    const hasSelected = selectedRowKeys.length > 0;

    function title() {
      return (
        <Row>
          <Col span={16}>
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
              <IButton type="danger" disabled={!hasSelected} perm="role:remove"  icon="delete">删除</IButton>
            </Popconfirm>
            <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入任务名称查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <div>
          <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({id:record.id})}>
            <IButton perm="role:remove" a> 删除 </IButton>
          </Popconfirm>
      </div>
    )

    const getContent = (record) => {
      return `
        修改对象:${record.tableInfo}<br />修改时间:${record.mtime}<br />修改内容：<br />${record.content}
      `
    }
    return (
      <Table
        columns={columns(toolBar)}
        expandedRowRender={record => <p>{<div dangerouslySetInnerHTML={{__html:getContent(record)}}></div>}</p>}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        size="middle"
        scroll={{ y: table_height}}
        bordered
        rowKey="id"
        loading={loading.effects['modifyInfo/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )
  }
}
