const columns = (toolBar) => {
    return [{
        title: '操作用户',
        dataIndex: 'name',
        width: 100
    }, {
        title: '请求URL',
        dataIndex: 'url',
        width: 140
    }, {
        title: '操作表名',
        dataIndex: 'tableName',
        width: 200,
    }, {
        title: '操作对象',
        dataIndex: 'tableInfo',
    }, {
        title: '记录ID',
        dataIndex: 'key',
        width: 180
    }, {
        title: '请求IP',
        dataIndex: 'ip',
        width:180,
    }, {
        title: '创建日期',
        dataIndex: 'createTime',
        width: 180
    }, {
        title: '操作',
        key: 'operation',
        width: 100,
        fixed: 'right',
        render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
