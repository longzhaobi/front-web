import request from '@/utils/axios';
import qs from 'qs';
export async function fetch(params) {
  return request({
    url:'/api/anon/classifications',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/classifications',
    method:'post',
    data:params
  });
}

export async function update(params) {
  return request({
    url:`/api/classifications`,
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:`/api/classifications`,
    method:'delete',
    params
  });
}
