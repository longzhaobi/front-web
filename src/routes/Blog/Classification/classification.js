import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

import {message} from 'antd';
export default modelExtend(grid(service, null, 'classification'), {
  namespace: 'classification',

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const o = yield select(({ classification }) => classification);
      const params = {
        current: o.current,
        size: o.size,
        keyword: o.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    },

  }
});
