import * as service from './api';

import modelExtend from 'dva-model-extend';
import grid from '@/models/grid';

import {message} from 'antd';
export default modelExtend(grid(service, null, 'article'), {
  namespace: 'article',

  effects: {
    *fetch({ payload }, { call, put, select }) {
      const o = yield select(({ article }) => article);
      const params = {
        current: o.current,
        size: o.size,
        keyword: o.keyword,
        ...payload
      }
      yield put({ type: 'superFetch', payload: params });
    },
    *queryById({ payload }, { put, select, call }) {
      return yield call(service.queryById,payload)
    },
    *confirm({ payload }, { put, select, call }) {
      return yield call(service.confirm, payload.id);
    },
    *reload(action, { put, select }) {
      const current = yield select(state => state.article.current);
      yield put({ type: 'fetch', payload: { current } });
    },
    *topHandler({ payload }, { put, select, call }) {
      const data = yield call(service.topHandler, payload);
      yield put({ type: 'app/result', payload: { data, namespace: 'article' } });
    },
    *fetchClassifications({ payload }, { call, put, select}) {
      return yield call(service.fetchClassifications);
    },
    *create({payload:params}, { call, put }) {
      return yield call(service.create, params);
    },
  }
});
