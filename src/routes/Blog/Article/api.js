import request from '@/utils/axios';
import qs from 'qs';
export async function fetch(params) {
  return request({
    url:'/api/articles/read/page',
    params
  });
}

export async function create(params) {
  return request({
    url:'/api/articles',
    method:'post',
    data:params
  });
}

export async function confirm(id) {
  return request({
    url:'api/articles/confirm',
    method:'put',
    data:{id}
  });
}

export async function update(params) {
  return request({
    url:'/api/articles',
    method:'put',
    data:params
  });
}

export async function remove(params) {
  return request({
    url:'/api/articles',
    method:'delete',
    params
  });
}

export async function queryById(id) {
  return request({
    url:'/api/anon/articles/read/detail',
    params:{id}
  });
}

export async function topHandler(id) {
  return request({
    url:'/api/top/articles',
    method:'put',
    data:{id}
  });
}

export async function fetchClassifications() {
  return request({
    url:'/api/anon/classifications/read/list'
  });
}

export async function fetchById(params) {
  return request({
    url: '/api/anon/articles',
    params
  });
}

