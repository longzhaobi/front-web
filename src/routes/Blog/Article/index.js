import React, {PropTypes, PureComponent} from 'react';
import { routerRedux } from 'dva/router';
import {connect} from 'dva';
import {Table, Select, Input, Button, Row, Col, Popconfirm, Icon, Divider, Message} from 'antd';
const Search = Input.Search;

import styles from './index.css';

import columns from './columns';

import IButton from '@/components/IButton';
import WithList from '@/hocs/WithList';

import queryString from 'query-string';

@connect((state) => ({
  ...state.article,
  namespace: 'article',
  loading: state.loading,
}))
@WithList({pathname: 'article/fetch'})
export default class Article extends PureComponent {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    const {namespace, dispatch} = this.props;
    dispatch({type:`${namespace}/fetch`});
  }

  confirmHandler = (id) => {
    const {namespace, dispatch} = this.props;
    dispatch({
      type: `${namespace}/confirm`,
      payload: id
    }).then((data) => {
      if(data) {
        Message.success('操作成功');
        dispatch({type:`${namespace}/fetch`});
      }
    })
  }

  topHandler = (id) => {
    const {namespace, dispatch} = this.props;
    dispatch({
      type: `${namespace}/topHandler`,
      payload: id
    })
  }

  render () {
    const { data, loading, selectedRowKeys, dispatch, namespace, keyword, removeHandler, onSearch, onChange, page, rowSelection } = this.props;

    const hasSelected = selectedRowKeys.length > 0;

    function openIssue(record = {}) {
      dispatch(routerRedux.push({
        pathname: '/blog/issue',
        search:queryString.stringify(record),
      }));
    }

    function title() {
      return (
        <Row>
          <Col span={16}>
            <IButton type="primary" icon="plus" perm="article:create" onClick={() => openIssue()}>发布博文</IButton>
            <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler(selectedRowKeys)}>
              <IButton type="danger" disabled={!hasSelected} icon="delete" perm="article:remove">删除</IButton>
            </Popconfirm>
            <span style={{ marginLeft: 8 }}>{hasSelected ? `选择了 ${selectedRowKeys.length} 条数据` : ''}</span>
          </Col>
          <Col span={8} style={{float:'right'}} >
            <Search size="default" style={{width:300,float:'right'}} defaultValue={keyword} placeholder="输入权限名称或标识查询..." onSearch={value => onSearch(value)} />
          </Col>
        </Row>
      )
    }

    const toolBar= (text, record, index) => (
      <span>
        <Popconfirm title={record.confirm === '1' ? '确定取消确认吗？' : '确定要确认该博文吗？'} onConfirm={() => this.confirmHandler({ id: record.id })}>
          <a href="javascript:void(0)">{record.confirm === '1' ? '取消确认' : '确认博文'}</a>
          <Divider type="vertical" />
        </Popconfirm>
        <IButton perm={`${namespace}:update`} onClick={() => openIssue({id: record.id})} a>
          编辑<Divider type="vertical" />
        </IButton>
        <Popconfirm title="确定要删除吗？" onConfirm={() => removeHandler({ id: record.id })}>
          <IButton perm={`${namespace}:remove`} a>删除 <Divider type="vertical" /></IButton>
        </Popconfirm>
        <Popconfirm title="确定要置顶吗？" onConfirm={() => this.topHandler(record.id)}>
          <a href="javascript:void(0)">{record.sort === '0' ? '置顶博文' : '取消置顶'}</a>
        </Popconfirm>
      </span>
    )

    return (
      <Table
        columns={columns(toolBar)}
        dataSource={data}
        pagination={false}
        rowSelection={rowSelection}
        scroll={{ y: table_height, x: 2000 }}
        bordered
        size="middle"
        rowKey="id"
        loading={loading.effects['article/superFetch']}
        title={() => title()}
        footer={() => page()}
      />
    )
  }

}
