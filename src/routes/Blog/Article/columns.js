import { Tag } from 'antd';
const columns = (toolBar) => {
    return [, {
      title: '是否确认',
      dataIndex: 'confirm',
      width: 120,
      fixed:'left',
      render: (text, record, index) => (
        text == '1' ? <Tag color="#2db7f5">已发布</Tag> : <Tag color="#f50">未发布</Tag>
      )
    },{
        title: '标题',
        dataIndex: 'title',
        width: 380
    }, {
        title: '作者',
        dataIndex: 'author',
        width: 180,
    }, {
        title: '原文链接',
        dataIndex: 'originalUrl',
        width: 380,
        render: (text, record, index) => (
            <span>{record.type == '1' ? `http://blog.loozb.com/articles/${record.id}` : text}</span>
        )
    }, {
        title: '阅读次数',
        dataIndex: 'readNum',
        width: 120,
    }, {
        title: '文章排序',
        dataIndex: 'sort',
        width: 120,
    }, {
        title: '文章类型',
        dataIndex: 'type',
        width: 120,
        render: (text, record, index) => (
            <span dangerouslySetInnerHTML={{ __html: text == '1' ? '原创' : '转载' }}></span>
        )
    }, {
        title: '创建日期',
        dataIndex: 'createTime',
        width: 180
    }, {
        title: '维护日期',
        dataIndex: 'updateTime',
        width: 180
    }, {
        title: '操作',
        key: 'operation',
        width: 260,
        fixed: 'right',
        render: (text, record, index) => toolBar(text, record, index)
    }]
}

export default columns;
