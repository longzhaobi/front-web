import React, { Component } from 'react';
import { Dragact } from 'dragact';
import Temp from './Temp';
export default class Dragacts extends React.Component {
  constructor(props) {
    super(props)
  }

  getblockStyle = (isDragging) => {
    return {
        background: isDragging ? '#1890ff' : 'white',
    }
  };
  render() {
    const fakeData = [
      { GridX: 0, GridY: 0, w: 4, h: 2, key: '0' },
      { GridX: 1, GridY: 0, w: 4, h: 2, key: '1' },
      { GridX: 2, GridY: 0, w: 4, h: 2, key: '2' },
      { GridX: 3, GridY: 0, w: 4, h: 2, key: '3' },
      { GridX: 4, GridY: 0, w: 4, h: 2, key: '4' },
    ]
    return (
      <Dragact
        layout={fakeData}//必填项
        col={16}//必填项
        width={800}//必填项
        rowHeight={40}//必填项
        margin={[5, 5]}//必填项
        className='plant-layout'//必填项
        style={{ background: '#333' }}//非必填项
        placeholder={true}//非必填项
      >
        {(item, provided) => {
            return (
              <div
                {...provided.props}
                {...provided.dragHandle}
                style={{
                    ...provided.props.style,
                    ...this.getblockStyle(provided.isDragging)
                }}
              >
                  <Temp />
              </div>
            )
        }}
      </Dragact>
    )
  }
}
