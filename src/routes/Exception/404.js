import React from 'react';
import { Link } from 'dva/router';
import Exception from '../../components/Exception';

export default function({location}) {
  const pathname = location.pathname;
  if(pathname.indexOf('-refresh') > -1) {
    return null;
  }

  return <Exception type="404" style={{ minHeight: 500, height: '80%' }} linkElement={Link} />;
}
