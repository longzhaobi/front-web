import React from 'react';
import queryString from 'query-string';
export default class NoSupport extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {location: { search }} = this.props;
    const query = queryString.parse(search);
    return (
      <div style={{width:700,margin:"300px auto"}}><h3>抱歉，该系统不支持您当前的浏览器版本{query ? query.version : null}，请使用IE11或者现代浏览器访问<a href="/#/user/login">登录页面</a></h3></div>
    )
  }
}
