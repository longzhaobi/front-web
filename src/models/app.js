import { hashHistory, routerRedux } from 'dva/router';
import * as service from '../services/app';
import JSEncrypt from 'jsencrypt';
import * as CryptoJS from 'crypto-js';

export default {

  namespace: 'app',

  state: {
    menu: [],
    menuStyle: localStorage.menuStyle || 'max',
    user: {},
    permission: [],
    online: 0,
    status: undefined,
    notices: [],
    fetchingNotices: false,
    notifyCount:0,
    initStatus: 'init'
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

  effects: {
    *login({ payload }, { put, select, call }) {
      //获取用户和密码，用来加密
      let { account, password } = payload;

      //使用公钥对密码进行加密
      const encrypt = new JSEncrypt();
      encrypt.setPublicKey(publicKey);
      //加密
      password = encrypt.encrypt(password);
      //对密码加密后进行登录操作
      return yield call(service.login, {account, password});
    },
    *logout({ payload }, { put, select, call }) {
      const pathname = yield select(state => state.routing.location.pathname);
      try {
        yield call(service.logout);//去后端清空登录信息
        const urlParams = new URL(window.location.href);
        urlParams.searchParams.set('redirect', pathname);
        window.history.replaceState(null, 'login', urlParams.href);
      } finally {
        yield put(routerRedux.push('/user/login'));
      }
    },
    *online(_, { put, select, call }) {
      const data = yield call(service.online);
      // console.log(data)
    },
    *current(_, {put, select, call}) {
      const data = yield call(service.current);
      if(data) {
        const { hasPermissions, sysUser} = data.data;
        localStorage.avatorPath = sysUser.avatar;
        localStorage.username = sysUser.username;
        localStorage.userId = sysUser.id;
        localStorage.permission = hasPermissions;
        yield put({type:'init', payload: {permission:hasPermissions, user:sysUser}})
      }
      yield put({type:'changeInitStatus', payload: 'ok'});
    },
    *fetchNotices(_, { call, put }) {
      const data = yield call(service.queryNotices);
      yield put({
        type: 'saveNotices',
        payload: data,
      });
      yield put({
        type: 'changeNotifyCount',
        payload: data.length,
      });
    },
    *clearNotices({ payload }, { put, select }) {
      yield put({
        type: 'saveClearedNotices',
        payload,
      });
      const count = yield select(state => state.app.notices.length);
      yield put({
        type: 'changeNotifyCount',
        payload: count,
      });
    },
    *feedback({ payload }, { put, select, call }) {
      return yield call(service.feedback, payload);
    },
    *fetchDics({ payload }, { call, put }) {
      return yield call(service.fetchDics, payload);
    },
    *fetchApprover({ payload }, { call, put }) {
      return yield call(service.fetchApprover, payload);
    },
    *queryTaskList({ payload }, { call, put }) {
      return yield call(service.queryTaskList, payload);
    },
    
  },

  reducers: {
    save(state, action) {
      return { ...state, ...action.payload };
    },
    clearLoginInfo(state, action) {
      return { ...state, initStatus: 'init', notifyCount: 0, permission:[], user:{}, menu: []};
    },
    switchClick(state, action) {
      return { ...state, menuStyle: action.payload }
    },
    init(state, {payload}) {
      return { ...state, ...payload}
    },
    changeInitStatus(state, {payload}) {
      return {...state, initStatus: payload};
    },
    saveNotices(state, { payload }) {
      return {
        ...state,
        notices: payload,
        fetchingNotices: false,
      };
    },
    saveClearedNotices(state, { payload }) {
      return {
        ...state,
        notices: state.notices.filter(item => item.type !== payload),
      };
    },
    changeNoticeLoading(state, { payload }) {
      return {
        ...state,
        fetchingNotices: payload,
      };
    },
    changeNotifyCount(state, action) {
      return {...state, notifyCount: action.payload,};
    },
  },

};
