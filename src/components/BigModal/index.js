import React from 'react';
import styles from './index.less';

/**
* Tab标签控制，一个路由对应一个标签
*/
export default class BigModal extends React.Component {
	constructor(props) {
    super(props);
  }

  render() {
    const { title = '表单', visible = false, children } = this.props;
    if(visible) {
      return (
        <div className={styles['big-modal-wrap']}>
          <div className={styles['big-modal']} >
            <div className={styles['big-modal-title']}>
              {title}
            </div>
            <div className={styles['big-modal-body']}>
              { children }
            </div>
            <div className={styles['big-modal-footer']}>
              footer
            </div>
          </div>
        </div>
      )
    }
    return null;
  }
}
