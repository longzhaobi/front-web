import React, { Component } from 'react';

import { Input, Modal, Button, message, Icon , Upload, Alert, notification, Select } from 'antd';
const Option = Select.Option;
class ImportModal extends Component {

  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
    this.state = {
      visible: false,
      loading: false,
      errorMsg:null,
      successMsg: null,
      uploadType:'1'
    };
  }

  showModelHandler = (e) => {
    if (e) e.stopPropagation();
    this.setState({
      visible: true,
      errorMsg:null,
      successMsg: null
    });
  };

  hideModelHandler = () => {
    this.setState({
      visible: false,
    });
  };

  handleChange = (uploadType) => {
    this.setState({uploadType})
  }

  okHandler = () => {

  };


  render() {
    const { children, style, title = 'Excel 数据导入', action } = this.props;
    const { loading, visible, uploadType } = this.state;
    function btnClick(key) {
      notification.close(key);
    }
    const _this = this;
    const props = {
      name: 'excelFile',
      showUploadList:false,
      action,
      data:{uploadType},
      onChange(info) {
        if (info.file.status === 'uploading') {
          // console.log(info.file, info.fileList);
          if(!_this.state.loading) {
            _this.setState({loading:true});
          }
          if(_this.state.errorMsg !== null || _this.state.successMsg !== null) {
            _this.setState({
              errorMsg:null,successMsg:null
            })
          }
        }
        if (info.file.status === 'done') {
          _this.setState({loading:false});
          const response = info.file.response;
          if(response.httpCode !== 200) {
            _this.setState({errorMsg: response.data})
          } else {
            const response = info.file.response;
            const msg = (<div dangerouslySetInnerHTML={{__html: response.data}}></div>)
            _this.setState({successMsg: msg ?  msg : '文件批处理成功'});
          }
        } else if (info.file.status === 'error') {
          _this.setState({loading:false});
          _this.setState({errorMsg: `文件【${info.file.name}】 上传失败`})
        }
      },
    };

    const message = (
      <span>上传Excel文件成功后，系统会自动进行入库，请耐心等待。（注：Excel文件表头必须与系统表格的表头名称一致，否则无法入库）</span>
    )

    return (
      <span>
        <span onClick={this.showModelHandler} style={style}>
          { children }
        </span>
        <Modal
          title= {title}
          zIndex={9999}
          visible={this.state.visible}
          width={720}
          maskClosable={false}
          onOk={this.okHandler}
          onCancel={this.hideModelHandler}
          footer={[
          <Button key="back" type="ghost" size="large" onClick={this.hideModelHandler}>关闭</Button>,
        ]}
        >
          <Alert showIcon message={message} type="info"></Alert>
          <Select defaultValue={uploadType} style={{ marginRight:8 }} dropdownStyle={{zIndex:99999}} onChange={this.handleChange}>
            <Option value="1">遇到错误停止入库</Option>
            <Option value="2">遇到错误继续入库</Option>
          </Select>
          <Upload {...props}>
            <Button loading={loading} disabled={loading}>
              {loading ? '正在处理数据，请等待...' : <span><Icon type="upload" /> 点击上传需要入库的EXCEL文件</span>}
            </Button>
          </Upload>
          {
            this.state.errorMsg != null && <Alert showIcon style={{marginTop:10}} message="错误信息:" closable onClose={() => this.setState({errorMsg: null})} description={this.state.errorMsg} type="error"></Alert>
          }
          {
            this.state.successMsg != null && <Alert showIcon style={{marginTop:10}} message="提示信息:" closable onClose={() => this.setState({successMsg: null})} description={this.state.successMsg} type="success"></Alert>
          }
        </Modal>
      </span>
    );
  }
}

export default ImportModal;
