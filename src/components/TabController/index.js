import React from 'react';
import { Tabs, message, Button, Dropdown, Icon, Menu } from 'antd';
const TabPane = Tabs.TabPane;
import { routerRedux } from 'dva/router';
import styles from './index.less';
/**
* Tab标签控制，一个路由对应一个标签
*/
export default class TabController extends React.Component {
	constructor(props) {
		super(props);
		const { home: {component, name = '首页'}} = props;
		this.state = {
			activeKey: null,
      panes: [{
				name,
				closable: false,
				key:'/home',
				component,
			}],
		};
  }

	componentWillMount() {
    const { name, keys, component } = this.props;
    if(keys === '/' || !name || keys === '/home') {
      return;
    }
		const panes = this.state.panes;
		const activeKey = keys;
		panes.push({ name, key: activeKey, component });
		this.setState({ panes, activeKey });
	}

	componentWillReceiveProps(nextProps) {
		const { name, keys, component } = nextProps;
    if(keys === '/' || !name) {
      return;
    }
		const panes = this.state.panes;
		const activeKey = keys;
		let isExist = false;
		for (let i = 0; i < panes.length; i++) {
			if (panes[i].key === activeKey) {
				isExist = true;
				break;
			}
		}

		if (isExist) {
			//如果已经存在
			this.setState({
				activeKey
			});
		} else {
			panes.push({ name, key: activeKey, component });
			this.setState({ panes, activeKey });
		}
	}

	onChange = (activeKey) => {
    // this.setState({ activeKey });
    this.props.dispatch(routerRedux.push({
      pathname: activeKey,
    }))
	}

	onEdit = (targetKey, action) => {
		this[action](targetKey);
	}

	remove = (targetKey) => {
		let activeKey = this.state.activeKey;
		let lastIndex;
		this.state.panes.forEach((pane, i) => {
			if (pane.key === targetKey) {
				lastIndex = i - 1;
			}
		});
		const panes = this.state.panes.filter(pane => pane.key !== targetKey);
		if (lastIndex >= 0 && activeKey === targetKey) {
			activeKey = panes[lastIndex].key;
		}
		this.setState({ panes, activeKey });
	}

	handleMenuClick = ({ key }) => {
		if(key === '1') {
			//关闭全部标签，只留下/home
			const panes = this.state.panes.filter(pane => pane.key === '/home');
			this.setState({panes, activeKey:'/home'})
		} else if(key === '2') {
			//关闭除此之外的所有标签，但不含首页

			//获取当前打开的标签
			const activeKey = this.state.activeKey;
			const panes = this.state.panes.filter(pane => pane.key === activeKey || pane.key === '/home');
			this.setState({ panes })
		}
  }

  onRefreshClick = () => {
    console.log(this.state.activeKey)
    this.props.dispatch(routerRedux.push({
      pathname: this.state.activeKey + '-refresh',
    }))
  }

	render() {
		const { location, match } = this.props;

		const menu = (
			<Menu onClick={this.handleMenuClick}>
				<Menu.Item key="1">关闭全部选项卡</Menu.Item>
				<Menu.Item key="2">关闭其他选项卡</Menu.Item>
			</Menu>
		);

		const operations = <div>
                          <Button icon="sync" onClick={this.onRefreshClick}>刷新</Button>
                          <Dropdown overlay={menu}>
                            <Button style={{ marginLeft: 8 }}>
                            <Icon type="close-square-o" />	关闭操作 <Icon type="down" />
                            </Button>
                          </Dropdown>
                        </div>


		return (
			<div className={styles['tab-box']}>
				<Tabs
					hideAdd
					onChange={this.onChange}
					activeKey={this.state.activeKey}
					type="editable-card"
					tabBarGutter={3}
					tabBarExtraContent={operations}
					onEdit={this.onEdit}
				>
					{this.state.panes.map(pane => <TabPane tab={pane.name} closable={pane.closable} key={pane.key} style={{height: 'calc(100vh - 102px)',overflowY:'auto'}}>
            <pane.component location={location} match={match}/>
          </TabPane>)}
				</Tabs>
			</div>
		)
	}
}
