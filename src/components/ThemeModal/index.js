import React, { PureComponent, Fragment } from 'react';

import { Modal, Button, Select, List, Switch, Divider, Radio } from 'antd';

const Body = ({ children, title, style }) => (
  <div
    style={{
      padding: 15,
      ...style,
    }}
  >
    <h3 className={styles.bodyTitle}>{title}</h3>
    {children}
  </div>
);

class ThemeModal extends PureComponent {

  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
    this.state = {
      visible: false
    };
  }

  showModelHandler = (e) => {
    if (e) e.stopPropagation();
    this.setState({
      visible: true,
    });
  };

  hideModelHandler = () => {
    this.setState({
      visible: false,
    });
  };

  okHandler = () => {

  };


  render() {
    const { children, style, title = '系统主题风格设置' } = this.props;
    const { loading, visible, uploadType } = this.state;
    return (
      <span>
        <span onClick={this.showModelHandler} style={style}>
          { children }
        </span>
        <Modal
          title= {title}
          zIndex={9999}
          visible={this.state.visible}
          width={720}
          maskClosable={false}
          onOk={this.okHandler}
          onCancel={this.hideModelHandler}
          footer={null}
        >
        <Body
            title="整体风格设置"
            style={{
              paddingBottom: 10,
            }}
          >
            <RadioGroup
              onChange={({ target }) =>
                this.changeSetting('silderTheme', target.value)
              }
              value={this.state.silderTheme}
            >
              <Radio style={radioStyle} value="dark">
                <ColorBlock color="#002140" title="深色导航" />
              </Radio>
              <Radio style={radioStyle} value="ligth">
                <ColorBlock color="#E9E9E9" title="浅色导航" />
              </Radio>
            </RadioGroup>
            <ThemeColor
              value={this.state.themeColor}
              onChange={color => this.changeSetting('themeColor', color)}
            />
          </Body>
          <Divider style={{ margin: 0 }} />
          <Body title="导航设置 ">
            <LayoutSeting
              value={this.state.layout}
              onChange={layout => this.changeSetting('layout', layout)}
            />
            <List
              split={false}
              dataSource={this.getLayOutSetting()}
              renderItem={item => (
                <List.Item actions={item.action}>{item.title}</List.Item>
              )}
            />
          </Body>
          <Divider style={{ margin: 0 }} />
          <Body title="其他设置">
            <List
              split={false}
              dataSource={[
                {
                  title: '色弱模式',
                  action: [
                    <Select
                      value={this.state.colorWeak}
                      onSelect={value => this.changeSetting('colorWeak', value)}
                      style={{ width: 120 }}
                    >
                      <Select.Option value="open">打开</Select.Option>
                      <Select.Option value="colse">关闭</Select.Option>
                    </Select>,
                  ],
                },
              ]}
              renderItem={item => (
                <List.Item actions={item.action}>{item.title}</List.Item>
              )}
            />
          </Body>
        </Modal>
      </span>
    );
  }
}

export default ThemeModal;
