import React from 'react';

import { Drawer, Card,Timeline, Icon } from 'antd';

import styles from './index.less';
import { isTypeNode } from 'typescript';

export default class WorkflowPanel extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      loading: false,
      data:[],
    }
  }

  onShow = () => {
    this.setState({
      visible:true
    });
    this.loadData();
  }

  loadData = () => {
    //展开前去获取数据
    this.setState({loading:true});
    const { id, dispatch } = this.props;
    dispatch({type:'app/queryTaskList', payload: {id}}).then(data => {
      this.setState({loading: false});
      if(data) {
        this.setState({data:data.data});
      } else {
        this.onHide();
      }
    })
  }

  onHide = () => {
    this.setState({
      visible:false
    });
  }

  packageData = (datas) => datas.map(item => {
    return <Timeline.Item key={item.id}>
      <Card>
        {item.taskName && <div><h3 style={{display:'inline'}}>流程名称：</h3>{item.taskName}</div>}
        {item.optionName && <div><h3 style={{display:'inline'}}>操作人：</h3>{item.optionName}</div>}
        {item.startTime && <div><h3 style={{display:'inline'}}>开始时间：</h3>{item.startTime}</div>}
        {item.endTime && <div><h3 style={{display:'inline'}}>结束时间：</h3>{item.endTime}</div>}
        {item.comment && <div><h3 style={{display:'inline'}}>审批批注：</h3>{item.comment}</div>}
        {item.taskStatus && <div><h3 style={{display:'inline'}}>流程状态：</h3>{item.taskStatus}</div>}
      </Card>
    </Timeline.Item>
  })


  render() {
    const { children, title } = this.props;
    return (
      <div className={styles['workflow-box']}>
        <span onClick={this.onShow}>
          { children }
        </span>
        <Drawer
          title={title || '流程轴'}
          placement="right"
          closable={false}
          className={styles['drawer-box']}
          width={380}
          onClose={this.onHide}
          visible={this.state.visible}
        >
          <Card loading={this.state.loading} bordered={false}>
            <Timeline className={styles['timeline']}>
              {this.packageData(this.state.data)}
            </Timeline>
          </Card>
        </Drawer>
      </div>
    )
  }
}
