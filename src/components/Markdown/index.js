import React, { PropTypes, PureComponent } from 'react';
export default class Markdown extends PureComponent {
  constructor(props) {
    super(props)
  }

  changeText = (value) => {
    this.props.onChange(value)
  }

  componentDidMount() {
    let mditor =  Mditor.fromTextarea(document.getElementById('editor-mk'));
    const { defaultValue } = this.props;
    const _self = this;
    //获取或设置编辑器的值
    mditor.on('ready',function(){
			mditor.on('changed', function(){
        _self.changeText(mditor.value)
			});
      mditor.value = defaultValue;
    });
  }

  render() {
    return (
      <div style={{height:'100%'}}>
        <textarea name="editor-mk" id="editor-mk" style={{height:table_height}}></textarea>
      </div>
    )
  }
}
