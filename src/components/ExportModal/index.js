import React, { Component } from 'react';

import { Input, Modal, Button, message, Icon , DatePicker, Select, Menu, Form } from 'antd';
const Option = Select.Option;
const FormItem = Form.Item;
const MonthPicker = DatePicker.MonthPicker;
const RangePicker = DatePicker.RangePicker;
/**
 * 时间记录表弹出框
 */
class ExportModal extends Component {

  constructor(props) {
    super(props);
    this.dispatch = props.dispatch;
    this.state = {
      visible: false,
      loading: false,
    };
  }

  showModelHandler = (e) => {
    if (e) e.stopPropagation();
    this.setState({
      visible: true,
      loading: false
    });
  };

  hideModelHandler = () => {
    this.setState({
      visible: false,
      loading: false
    });
  };

  handleChange = () => {

  }

  handleSubmit = () => {
    this.props.form.validateFields((err, fieldsValue) => {
      if (!err) {
        const rangeValue = fieldsValue['range-picker'];
        const beginDate = rangeValue[0].format('YYYY-MM-DD');
        const endDate = rangeValue[1].format('YYYY-MM-DD');
        const href = `${fileUrl}/users/export?beginDate=${beginDate}&endDate=${endDate}`
        window.open(href);
      }
    })
  };

  render() {
    const { children, style, title = 'Excel 数据导出', action  } = this.props;
    const { loading, visible } = this.state;
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
      },
    };
    const config = {
      rules: [{ type: 'object', required: true, message: 'Please select time!' }],
    };
    const rangeConfig = {
      rules: [{ type: 'array', required: true, message: 'Please select time!' }],
    };

    return (
      <span>
        <span onClick={this.showModelHandler} style={style}>
          { children }
        </span>
        <Modal
          title={title}
          visible={this.state.visible}
          width={600}
          maskClosable={false}
          onCancel={this.hideModelHandler}
          footer={null}
        >
          <Form>
            {/* <FormItem
              {...formItemLayout}
              label="导出规则"
            >
              <Select defaultValue="1" style={{ marginRight:8, width:200 }} dropdownStyle={{zIndex:99999}} onChange={this.handleChange}>
                <Option value="1">按数据日期导出</Option>
              </Select>
            </FormItem> */}
            <FormItem
              {...formItemLayout}
              label="导出日期范围"
            >
              {getFieldDecorator('range-picker', rangeConfig)(
                <RangePicker popupStyle={{zIndex:99999}}/>
              )}
            </FormItem>
            <FormItem
              wrapperCol={{
                xs: { span: 24, offset: 0 },
                sm: { span: 14, offset: 10 },
              }}
            >
              <Button type="primary" onClick={this.handleSubmit}>导出</Button>
            </FormItem>
          </Form>
        </Modal>
      </span>
    );
  }
}

export default Form.create()(ExportModal);
