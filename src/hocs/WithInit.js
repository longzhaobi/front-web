/**
* 高阶组件，初始化登录信息
*/
import React from 'react';
import { Spin  } from 'antd';
import { Link } from 'dva/router';
// import io from 'socket.io-client';


const WithInit = (WrappedComponent) => {

	return class HOC extends React.Component {
		componentWillMount() {
      //去初始化信息前，先清空浏览器本地缓存
      const signToken = localStorage.signToken;
      localStorage.clear();
      localStorage.signToken = signToken;
      this.props.dispatch({type:'app/current'});
		}


		render() {
      const {app: {initStatus}} = this.props;
			if (initStatus === 'ok') {
				return (
					<WrappedComponent {...this.props} />
				)
			}
			return (
				<Spin tip="初始化中，请稍后..." style={{ marginTop: '20%' }}>
					<div style={{ width: '100%', background: 'rgba(50,50,50, 0.1)' }}></div>
				</Spin>
			)
		}
	}
}

export default WithInit;
