/**
* 高阶组件，判断浏览器版本
*/
import React from 'react';
import { Spin  } from 'antd';
import { Link } from 'dva/router';
import { routerRedux } from 'dva/router';
import queryString from 'query-string';

const WithExplore = (WrappedComponent) => {

	return class HOC extends React.Component {
    constructor(props) {
      super(props);
    }
		componentWillMount() {
      const version = this.getExplore();
      //去初始化信息前，先清空浏览器本地缓存
      if(version.indexOf('IE') > -1) {
        //如果当前用户IE版本是IE，且不是IE11，则直接跳转
        if(version.indexOf("11") === -1) {
          this.props.dispatch(routerRedux.push({
            pathname: '/blank/nosupport',
            search: queryString.stringify({ version }),
          }))
          return;
        }
      }
    }

    getExplore = () => {
      var sys = {},
        ua = navigator.userAgent.toLowerCase(),
        s;
        (s = ua.match(/rv:([\d.]+)\) like gecko/)) ? sys.ie = s[1]:
            (s = ua.match(/msie ([\d\.]+)/)) ? sys.ie = s[1] :
            (s = ua.match(/edge\/([\d\.]+)/)) ? sys.edge = s[1] :
            (s = ua.match(/firefox\/([\d\.]+)/)) ? sys.firefox = s[1] :
            (s = ua.match(/(?:opera|opr).([\d\.]+)/)) ? sys.opera = s[1] :
            (s = ua.match(/chrome\/([\d\.]+)/)) ? sys.chrome = s[1] :
            (s = ua.match(/version\/([\d\.]+).*safari/)) ? sys.safari = s[1] : 0;
        // 根据关系进行判断
        if (sys.ie) return ('IE: ' + sys.ie)
        if (sys.edge) return ('EDGE: ' + sys.edge)
        if (sys.firefox) return ('Firefox: ' + sys.firefox)
        if (sys.chrome) return ('Chrome: ' + sys.chrome)
        if (sys.opera) return ('Opera: ' + sys.opera)
        if (sys.safari) return ('Safari: ' + sys.safari)
        return 'Unkonwn'
    }


		render() {
			return (
				<WrappedComponent {...this.props} />
			)
		}
	}
}

export default WithExplore;
