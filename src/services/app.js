import request from '../utils/axios';
import fetch from '../utils/request';
import qs from 'qs';
export async function query() {
  return request('/api/users');
}

export function issue_public_key() {
  return request({
    url:'/api/issuePublicKey',
    method: 'get',
  });
}

export function login(params) {
  return request({
    url:'/api/login',
    method: 'post',
    data: params
  });
}

//写入前端错误信息
export function saveErrorsInfo(params) {
  return request({
    url:'/api/errorInfos',
    method:'post',
    data:params
  });
}

export function register(params) {
  return request({
    url:'/api/register',
    method:'post',
    data:params
  });
}

export function logout() {
  return request({
    url:'/api/logout',
    method: 'post'
  });
}

export function current() {
  return request({
    url:'/api/users/current'
  });
}

export function verifyAuth(params) {
  return request({
    url:'/api/users/verifyAuth',
    params
  });
}

//获取数据字典
export function fetchDics(params) {
  return request({
    url:'/api/dics/fetchDics',
    params
  });
}

export function fetchApprover(params) {
  return request({
    url:'/api/users/approver',
    params
  });
}


export function updatePassword(params) {
  return request({
    url:`/api/users/updatePassword`,
    method:'put',
    data:params
  });
}

export function online(params) {
  return request({
    url: '/api/online',
    method: 'get'
  });
}

export async function queryNotices() {
  return fetch('/mock/notices');
}

export async function feedback(params) {
  return request({
    url: '/api/opinions',
    method: 'post',
    data: params
  });
}

export async function queryTaskList(params) {
  return request({
    url: '/api/workflow/read/task/list',
    method: 'get',
    params
  });
}
