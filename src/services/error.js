import request from '../utils/request';

export async function query404() {
  return request('/mock/404');
}

export async function query401() {
  return request('/mock/401');
}

export async function query403() {
  return request('/mock/403');
}

export async function query500() {
  return request('/mock/500');
}
