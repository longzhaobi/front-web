import { stringify } from 'qs';
import request from '../utils/request';

export async function queryProjectNotice() {
  return request('/mock/project/notice');
}

export async function queryActivities() {
  return request('/mock/activities');
}

export async function queryRule(params) {
  return request(`/mock/rule?${stringify(params)}`);
}

export async function removeRule(params) {
  return request('/mock/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return request('/mock/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function fakeSubmitForm(params) {
  return request('/mock/forms', {
    method: 'POST',
    body: params,
  });
}

export async function fakeChartData() {
  return request('/mock/fake_chart_data');
}

export async function queryTags() {
  return request('/mock/tags');
}

export async function queryBasicProfile() {
  return request('/mock/profile/basic');
}

export async function queryAdvancedProfile() {
  return request('/mock/profile/advanced');
}

export async function queryFakeList(params) {
  return request(`/mock/fake_list?${stringify(params)}`);
}

export async function fakeAccountLogin(params) {
  return request('/mock/login/account', {
    method: 'POST',
    body: params,
  });
}

export async function fakeRegister(params) {
  return request('/mock/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return request('/mock/notices');
}
