import request from '../utils/request';

export async function queryProvince() {
  return request('/mock/geographic/province');
}

export async function queryCity(province) {
  return request(`/mock/geographic/city/${province}`);
}
