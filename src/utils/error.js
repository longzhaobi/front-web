import { routerRedux } from 'dva/router';

const error = (e, dispatch) => {
  //先关闭初始化
  dispatch({type:'app/changeInitStatus', payload: 'ok'});
  if(e.response) {
    let {data, status, statusText, config} = e.response;
    const { httpCode } = data;
    if(httpCode) {
      status = httpCode;
    }
    //网络连接超时
    if(status === 504) {
      dispatch(routerRedux.push('/exception/504'));
      return;
    }
    //未登录
    if (status === 401) {
      dispatch(routerRedux.push('/user/login'));
      return;
    }
    //没有权限
    if (status === 403 && config.method === 'get') {
      dispatch(routerRedux.push('/exception/403'));
      return;
    }
    // if (status <= 504 && status >= 500) {
    //   dispatch(routerRedux.push('/exception/500'));
    //   return;
    // }
    if (status >= 404 && status < 422 && config.method === 'get') {
      dispatch(routerRedux.push('/exception/404'));
    }
  } else {
    dispatch(routerRedux.push('/exception/504'));
  }
};

export default error;
