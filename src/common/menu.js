import {
  isUrl
} from '../utils/utils';

const menuData = [
  // {
  //   name: 'Dashboard',
  //   icon: 'dashboard',
  //   hide: true,
  //   path: 'dashboard',
  //   children: [{
  //     name: '分析页',
  //     path: 'analysis',
  //   }, {
  //     name: '监控页',
  //     path: 'monitor',
  //   }, {
  //     name: '工作台',
  //     path: 'workplace',
  //   }],
  // },
  {
    name: '博客管理',
    icon: 'book',
    path: 'blog',
    key: 'blog',
    children: [{
      name: '博文列表',
      path: 'article',
      key: 'article',
      icon: 'book',
    }, {
      name: '博文类型',
      path: 'classification',
      key: 'classification',
      icon: 'tags-o',
    }, {
      name: '博文编辑',
      path: 'issue',
      icon: 'book',
      key: 'issue',
      hide: true,
    }],
  }, {
    name: '系统管理',
    icon: 'user',
    key: 'admin',
    path: 'admin',
    children: [{
      name: '系统检测',
      path: 'system',
      key: 'system',
      icon: 'trademark'
    }, {
      name: '角色管理',
      path: 'role',
      key: 'role',
      icon: 'trademark'
    }, {
      name: '用户管理',
      path: 'user',
      key: 'user',
      icon: 'user-add'
    }, {
      name: '组织管理',
      path: 'organ',
      key: 'organ',
      icon: 'team'
    }, {
      name: '资源管理',
      path: 'resource',
      key: 'resource',
      icon: 'database'
    }, {
      name: '权限管理',
      path: 'permission',
      key: 'permission',
      icon: 'safety'
    }, {
      name: '会话管理',
      path: 'session',
      key: 'session',
      icon: 'trademark'
    }, {
      name: '数据字典',
      path: 'dic',
      key: 'dic',
      icon: 'database'
    }, {
      name: '缓存管理',
      path: 'cache',
      key: 'cache',
      icon: 'wallet'
    }, {
      name: '意见反馈',
      path: 'opinion',
      key: 'opinion',
      icon: 'notification'
    }, {
      name: '附件管理',
      path: 'file',
      key: 'file',
      icon: 'file-add'
    }],
  }, {
    name: '日志管理',
    icon: 'file-text',
    path: 'logger',
    key: 'logger',
    children: [{
      name: '业务日志',
      path: 'event',
      key: 'event',
      icon: 'code-o'
    }, {
      name: '系统日志',
      path: 'sysLog',
      key: 'sysLog',
      icon: 'copy'
    }, {
      name: '修改日志',
      path: 'modifyInfo',
      key: 'modifyInfo',
      icon: 'edit'
    }, {
      name: '登录日志',
      path: 'loginLog',
      key: 'loginLog',
      icon: 'trademark'
    }],
  },
  {
    name: '流程审批',
    icon: 'file-text',
    path: 'workflow',
    key: 'workflow',
    children: [{
      name: '任务信息',
      path: 'task',
      key: 'task',
      icon: 'code-o'
    }, {
      name: '请假信息',
      path: 'leave',
      key: 'leave',
      icon: 'copy'
    }, {
      name: '报销申请',
      path: 'expenseAccount',
      key: 'expenseAccount',
      icon: 'copy'
    }, {
      name: '薪水信息',
      path: 'salary',
      key: 'salary',
      icon: 'bank',
      children:[{
        name: '薪资调整',
        path: 'salaryAdjust',
        key: 'salaryAdjust',
        icon: 'edit'
      },{
        name: '员工薪水',
        path: 'employeeSalary',
        key: 'employeeSalary',
        icon: 'bank'
      }]
    }],
  },
  {
    name: '表单页',
    icon: 'form',
    hide: true,
    path: 'form',
    children: [{
      name: '基础表单',
      path: 'basic-form',
    }, {
      name: '分步表单',
      path: 'step-form',
    }, {
      name: '高级表单',
      authority: 'admin',
      path: 'advanced-form',
    }],
  }, {
    name: '列表页',
    icon: 'table',
    hide: true,
    path: 'list',
    children: [{
      name: '查询表格',
      path: 'table-list',
    }, {
      name: '标准列表',
      path: 'basic-list',
    }, {
      name: '卡片列表',
      path: 'card-list',
    }, {
      name: '搜索列表',
      path: 'search',
      children: [{
        name: '搜索列表（文章）',
        path: 'articles',
      }, {
        name: '搜索列表（项目）',
        path: 'projects',
      }, {
        name: '搜索列表（应用）',
        path: 'applications',
      }],
    }],
  }, {
    name: '详情页',
    icon: 'profile',
    hide: true,
    path: 'profile',
    children: [{
      name: '基础详情页',
      path: 'basic',
    }, {
      name: '高级详情页',
      path: 'advanced',
      authority: 'admin',
    }],
  }, {
    name: '结果页',
    icon: 'check-circle-o',
    path: 'result',
    children: [{
      name: '成功',
      path: 'success',
    }, {
      name: '失败',
      path: 'fail',
    }],
  }, {
    name: '异常页',
    icon: 'warning',
    path: 'exception',
    children: [{
      name: '403',
      path: '403',
    }, {
      name: '404',
      path: '404',
    }, {
      name: '500',
      path: '500',
    }, {
      name: '504',
      path: '504',
    }, {
      name: '触发异常',
      path: 'trigger',
    }],
  }, {
    name: '账户',
    icon: 'user',
    path: 'user',
    hide: true,
    authority: 'guest',
    children: [{
      name: '登录',
      path: 'login',
    }, {
      name: '注册',
      path: 'register',
    }, {
      name: '注册结果',
      path: 'register-result',
    }],
  }, {
    name: '其他组件',
    icon: 'export',
    path: 'other',
    authority: 'guest',
    children: [{
      name: '拖拽布局',
      path: 'dragact',
    }],
  }, {
    name: '首页',
    icon: 'home',
    hide: true,
    path: 'home',
  }, {
    name: '使用文档',
    icon: 'book',
    path: 'http://pro.ant.design/docs/getting-started',
    target: '_blank',
  }
];

function formatter(data, parentPath = '', parentAuthority) {
  return data.map((item) => {
    let {
      path
    } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
    };
    if (item.children) {
      result.children = formatter(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
}

export const getMenuData = () => formatter(menuData);
