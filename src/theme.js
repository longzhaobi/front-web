// https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less
module.exports = {
  'primary-color': '#4397e6',
  'card-actions-background': '#f5f8fa',
};
