echo "开始构建"

#安装依赖   因阿里云服务器配置太低，只有本地打包后上传执行下面未注释的步骤。
#yarn install
#编码源码
#npm run build
#获取镜像信息
imagesId=`docker images|grep -i front-web|awk '{print $3}'`

#判断镜像是否存在，如果存在强行删除
if ! -n "$imagesId";then
  echo "当前镜像不存在"
else
  docker rmi $imagesId -f
fi

#构建镜像
docker build -t front-web .

#判断容器是否存在，如果存在删除
if docker ps -a|grep -i front-web;then
   docker rm -f front-web
fi

#启动容器
docker run -p 96:80 --name front-web -d front-web
